#ifndef BCPROJECT_INTERVALGRAPH_HPP
#define BCPROJECT_INTERVALGRAPH_HPP

///\cond DO_NOT_DOCUMENT
#include <boost/graph/graph_traits.hpp>
#include <boost/icl/interval.hpp>
#include <boost/concept/assert.hpp>
#include <boost/graph/graph_concepts.hpp>
#include <boost/icl/dynamic_interval_traits.hpp>
#include <boost/iterator/iterator_concepts.hpp>
#include <vector>
///\endcond

namespace boost {
    /**
     * @file
     * @brief Constructs interval graph based on provided list of intervals
     * @tparam Graph type of output graph structure, must provide Mutable Graph Concept
     * @tparam Iter forward iterator of a container containing intervals<br>
     * @tparam IntervalMap type of vertex property map, key type must be Graph vertex descriptor,
     * value type must be an interval from Interval Container Library
     * @param[out] g output graph to which vertices and edges will be added in order to create interval graph
     * @param[in] begin iterator pointing to the beginning of a container of intervals
     * @param[in] end iterator pointing to the end of the container of intervals
     * @param[out] interval_map vertex property map of graph <b>g</b>
     */
    template<class Graph, class Iter, class IntervalMap>
    void construct_interval_graph(Graph &g, Iter begin, Iter end, IntervalMap interval_map) {
        typedef typename boost::property_traits<IntervalMap>::value_type value_type;
        typedef typename boost::property_traits<IntervalMap>::key_type key_type;
        typedef typename boost::property_traits<IntervalMap>::category category;
        typedef typename boost::graph_traits<Graph>::vertex_descriptor VertexDescriptor;

        BOOST_CONCEPT_ASSERT((boost::concepts::MutableGraphConcept<Graph>));
        BOOST_STATIC_ASSERT((boost::is_property_map<IntervalMap>::value));
        BOOST_STATIC_ASSERT((boost::is_same<key_type, VertexDescriptor>::value));
        BOOST_STATIC_ASSERT((boost::icl::is_interval<value_type>::value));
        BOOST_STATIC_ASSERT((boost::is_convertible<category, boost::writable_property_map_tag>::value));
        BOOST_STATIC_ASSERT_MSG((boost::is_undirected_graph<Graph>::value), "Graph must be undirected!");
        BOOST_STATIC_ASSERT((boost::icl::is_interval<typename std::iterator_traits<Iter>::value_type>::value));
        BOOST_STATIC_ASSERT(
                (boost::is_convertible<typename boost::iterator_traversal<Iter>::type, boost::forward_traversal_tag>::value));

        // Add vertices to graph
        std::vector<VertexDescriptor> vertices_vector;
        for (auto it = begin; it != end; it++) {
            vertices_vector.push_back(add_vertex(g));
            interval_map[*(vertices_vector.rbegin())] = *it;
        }

        // For each pair of intervals check if they intersect and if yes, make a new edge between the two
        int index = 0;
        for (auto it = begin; it != end; it++) {
            int index2 = index;
            for (auto it2 = it; it2 != end; it2++) {
                if (it == it2) {
                    index2++;
                    continue;
                }

                if (boost::icl::intersects(*it, *it2))
                    add_edge(vertices_vector[index], vertices_vector[index2], g);

                index2++;
            }
            index++;
        }

    }
}
#endif //INTERVAL_GRAPH_RECOGNITION_INTERVALGRAPH_HPP
