#ifndef INTERVAL_GRAPH_RECOGNITION_LEXBFS_HPP
#define INTERVAL_GRAPH_RECOGNITION_LEXBFS_HPP

///\cond DO_NOT_DOCUMENT
#include "internal/utility_functions.hpp"
#include <boost/concept/assert.hpp>
#include <boost/graph/graph_concepts.hpp>
///\endcond

namespace boost {
    using namespace internal;

    /**
     * @file
     * @brief Creates a Lex-BFS ordering of graph vertices
     * @tparam Graph type of input graph structure, must provide Vertex List Graph Concept and Adjacency Graph Concept
     * @tparam Inserter output insert iterator of a container to which Lex-BFS ordering or vertices will be stored
     * @param[in] g input graph
     * @param[out] ins insert iterator used for inserting lex-BFS vertex ordering to user’s structure
     */
    template<class Graph, class Inserter>
    void lex_bfs(const Graph &g, Inserter ins) {
        typedef typename boost::graph_traits<Graph>::vertex_descriptor VertexDesciptor;
        typedef vertex_element<VertexDesciptor> Element;
        typedef partition_class<Element> Partition;

        BOOST_CONCEPT_ASSERT((boost::concepts::VertexListGraphConcept<Graph>));
        BOOST_CONCEPT_ASSERT((boost::concepts::AdjacencyGraphConcept<Graph>));
        BOOST_STATIC_ASSERT_MSG((boost::is_undirected_graph<Graph>::value), "Graph must be undirected!");
        BOOST_STATIC_ASSERT((is_same<typename Inserter::iterator_category, std::output_iterator_tag>::value));
        BOOST_STATIC_ASSERT((boost::is_same<typename Inserter::container_type::value_type, VertexDesciptor>::value));

        // Return if given graph has no vertices
        if (vertices(g).first == vertices(g).second)
            return;

        // Define list of all vertices wrapped inside vertex_element (element) struct
        std::list<Element *> elements;
        construct_vertex_elements(g, vertices(g).first, vertices(g).second, elements);

        // Copy elements pointers to be able to delete them at the end
        std::list<Element *> elements_pointers_for_deletion = {};
        std::copy(elements.begin(), elements.end(),
                  std::inserter(elements_pointers_for_deletion, elements_pointers_for_deletion.begin()));

        // Add neighbours to elements according to graph edges
        add_neighbours_to_elements(g, elements);

        // Define partitions list and initialize it with first partition containing all elements
        std::list<Partition> partitions;
        partitions.emplace_back(Partition(elements.begin(), elements.end(), 0));
        partitions.begin()->back--;

        for (Element *vertex_it : elements)
            vertex_it->containing_partition_it = partitions.begin();

        // Define list for storage of ordered vertices throughout the algorithm
        std::list<VertexDesciptor> ordering;
        unsigned int current_time = 1;

        while (!elements.empty()) {
            // Select the last unprocessed element
            Element *pivot = *(partitions.rbegin()->back);
            pivot->visited = true;

            // Add him to the front of the final ordering
            ordering.emplace_front(pivot->vertex_descriptor);

            // Shrink the containing partition of pivot to the left
            if (partitions.rbegin()->begin == partitions.rbegin()->back)
                // Remove the partition from partitions if it would become empty
                partitions.pop_back();
            else
                partitions.rbegin()->back--;

            // Remove pivot from the list of elements
            elements.pop_back();

            // Do for each neighbour of pivot
            for (Element *neighbour : pivot->neighbours) {
                // If current neighbour was already processed as a pivot, select next neighbour
                if (neighbour->visited)
                    continue;

                split_partition_to_back(neighbour, partitions, current_time);
            }

            // Increase the creation time for the next iteration
            current_time++;
        }

        // Copy final ordering to the users ordering structure
        std::copy(ordering.begin(), ordering.end(), ins);

        // Delete created elements wrapping the graph vertices
        free_memory(elements_pointers_for_deletion);
    }
}
#endif //INTERVAL_GRAPH_RECOGNITION_LEXBFS_HPP
