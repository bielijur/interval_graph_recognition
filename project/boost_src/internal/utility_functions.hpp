#ifndef INTERVAL_GRAPH_RECOGNITION_UTILITYFUNCTIONS_HPP
#define INTERVAL_GRAPH_RECOGNITION_UTILITYFUNCTIONS_HPP

#include <deque>
#include "structures.hpp"
#include "../clique_t.hpp"
#include <boost/graph/connected_components.hpp>

namespace boost {
    namespace internal {
        // Wraps graph vertices in element_vertex struct
        template<class Graph, class Iter>
        void construct_vertex_elements(const Graph &g, Iter begin, Iter end,
                                       std::list<vertex_element<typename boost::graph_traits<Graph>::vertex_descriptor> *> &elements) {
            typedef typename boost::graph_traits<Graph>::vertex_descriptor Vertex;
            typedef vertex_element<Vertex> Element;

            for (auto vertex_it = begin; vertex_it != end; vertex_it++) {
                Element *new_element = new Element(*vertex_it);
                elements.emplace_back(new_element);
                new_element->list_position_it = std::prev(elements.end());
            }
        }

        // Adds neighbours to the elments according to graph edges
        template<class Graph>
        void add_neighbours_to_elements(const Graph &g,
                                        const std::list<vertex_element<typename boost::graph_traits<Graph>::vertex_descriptor> *> &elements) {
            typedef typename boost::graph_traits<Graph>::vertex_descriptor Vertex;
            typedef vertex_element<Vertex> Element;

            std::unordered_map<Vertex, Element *> element_map;

            // Insert elements to map with key being the vertex_descriptor and value being the vertex_element struct,
            // used for quick search
            for (Element *tmp_element : elements)
                element_map.insert(std::make_pair(tmp_element->vertex_descriptor, tmp_element));

            for (Element *element : elements) {
                for (auto adjacent_it = adjacent_vertices(element->vertex_descriptor, g).first;
                     adjacent_it != adjacent_vertices(element->vertex_descriptor, g).second; adjacent_it++) {
                    element->neighbours.emplace_back(element_map.find(*adjacent_it)->second);
                }
            }
        }

        // Adds RN to elements based on given vertices ordering
        template<class VertexDescriptor>
        void add_RN_to_elements(const std::list<vertex_element<VertexDescriptor> *> &elements) {
            typedef vertex_element<VertexDescriptor> Element;

            // Set Lex-BFS ordering pos to elements
            unsigned int pos_num = 0;
            for (Element *element : elements) {
                element->ordering_pos = pos_num;
                pos_num++;
            }

            for (Element *element : elements) {
                for (Element *neighbour : element->neighbours) {
                    if (neighbour->ordering_pos > element->ordering_pos) {
                        element->RN.push_back(neighbour);
                    }
                }
            }
        }

        // Counting sort is used for ordering RN
        // Used for sorting pairs (x, y), where x is vertex and y belongs to RN(x)
        static void
        counting_sort(std::vector<std::vector<unsigned int>> &input, unsigned int unique_count, unsigned int index) {
            typedef std::vector<std::vector<unsigned int>> Vector;

            std::vector<unsigned int> count(unique_count);

            Vector tmp_vector = input;
            for (auto pair : input) {
                count[pair[index]]++;
            }

            std::vector<unsigned int> beginning_position(unique_count);
            for (unsigned int i = 1; i < unique_count; i++)
                beginning_position[i] += beginning_position[i - 1] + count[i - 1];

            for (auto pair : tmp_vector) {
                input[beginning_position[pair[index]]] = pair;
                beginning_position[pair[index]]++;
            }
        }

        // Orders elements’ RN based on their Lex-BFS ordering
        template<class VertexDescriptor>
        void order_elements_RN(const typename std::list<vertex_element<VertexDescriptor> *> elements) {
            typedef vertex_element<VertexDescriptor> Element;

            std::vector<Element *> elements_vector;

            // Create pairs for sorting
            unsigned int elements_count = 0;
            std::vector<std::vector<unsigned int>> pairs_vector;

            for (Element *element : elements) {
                elements_vector.push_back(element);
                elements_count++;

                for (Element *rn_element : element->RN) {
                    std::vector<unsigned int> tmp;
                    tmp.push_back(element->ordering_pos);
                    tmp.push_back(rn_element->ordering_pos);
                    pairs_vector.push_back(tmp);
                }
            }

            counting_sort(pairs_vector, elements_count, 1);

            for (Element *element : elements_vector)
                element->RN = {};

            for (auto pair : pairs_vector) {
                elements_vector[pair[0]]->RN.push_back(elements_vector[pair[1]]);

                if (elements_vector[pair[0]]->parent == nullptr)
                    elements_vector[pair[0]]->parent = elements_vector[pair[1]];
            }
        }

        // Checks whether child’s RN/parent is a subset of parent’s RN
        template<class VertexDescriptor>
        bool
        is_RN_subset(const vertex_element<VertexDescriptor> *child, const vertex_element<VertexDescriptor> *parent) {
            if (parent == NULL)
                return true;

            auto child_RN_it = child->RN.begin();
            child_RN_it++;


            for (auto parent_RN_it = parent->RN.begin(); parent_RN_it != parent->RN.end(); parent_RN_it++) {
                if (child_RN_it == child->RN.end())
                    return true;

                // Skip parent in child’s RN list
                if (*child_RN_it == *parent_RN_it)
                    child_RN_it++;
            }

            return child_RN_it == child->RN.end();
        }

        // Constructs elements tree based on ordered RN parents
        template<class VertexDescriptor>
        void construct_element_tree(const typename std::list<vertex_element<VertexDescriptor> *> &elements) {
            typedef vertex_element<VertexDescriptor> Element;

            for (Element *element : elements) {
                if (element->parent != NULL)
                    element->parent->tree_children.push_back(element);
            }
        }

        // Constructs clique tree recursively
        template<class VertexDescriptor>
        void
        construct_clique_tree(std::list<clique<VertexDescriptor> *> &cliques, vertex_element<VertexDescriptor> *root,
                              unsigned int &index) {
            typedef vertex_element<VertexDescriptor> Element;
            typedef clique<VertexDescriptor> Clique;

            Clique *parent_clique = root->parent->containing_clique;

            if (!std::equal(root->RN.begin(), root->RN.end(), parent_clique->clique_elements.begin(),
                            parent_clique->clique_elements.end())) {
                index++;
                Clique *new_clique = new Clique(root);
                cliques.push_back(new_clique);

                new_clique->list_position_index = index;
                for (Element *root_rn_element : root->RN) {
                    new_clique->clique_elements.push_back(root_rn_element);
                    new_clique->clique_vertices.push_back(root_rn_element->vertex_descriptor);
                }
                root->containing_clique = new_clique;
                new_clique->list_position_it = std::prev(cliques.end());
                new_clique->parent = parent_clique;
                new_clique->parent->children.push_back(new_clique);

                new_clique->neighbours.push_back(parent_clique);
                parent_clique->neighbours.push_back(new_clique);

            } else {
                parent_clique->clique_elements.push_front(root);
                parent_clique->clique_vertices.push_front(root->vertex_descriptor);
                root->containing_clique = parent_clique;
            }

            for (Element *child : root->tree_children)
                construct_clique_tree(cliques, child, index);
        }

        // Auxiliary function for creating internal clique tree representation
        template<class Graph, class IT, class VertexDescriptor>
        void clique_tree_aux(const Graph &g, IT o_begin, IT o_end, std::list<clique<VertexDescriptor> *> &cliques,
                             std::list<vertex_element<VertexDescriptor> *> &elements) {
            typedef vertex_element<VertexDescriptor> Element;
            typedef clique<VertexDescriptor> Clique;

            // Return empty clique list if given graph has no vertices
            if (vertices(g).first == vertices(g).second) {
                cliques = {};
                return;
            }

            // Define list of all vertices wrapped inside vertex_element (element) struct, add them neighbours and ordered RN
            construct_vertex_elements(g, o_begin, o_end, elements);
            add_neighbours_to_elements(g, elements);
            add_RN_to_elements(elements);
            order_elements_RN(elements);

            // Construct element tree
            construct_element_tree(elements);

            // Construct clique tree from elements, using the last element as it’s root
            Element *root = elements.back();

            Clique *new_clique = new Clique(root);
            cliques.push_back(new_clique);
            root->containing_clique = new_clique;
            new_clique->list_position_it = cliques.begin();

            unsigned int index = 0;
            for (Element *child : root->tree_children)
                construct_clique_tree(cliques, child, index);
        }

        // Construct output graph from internal clique tree
        template<class VertexDescriptor, class CliqueTree, class CliqueMap>
        void
        construct_output(const std::list<clique<VertexDescriptor> *> &cliques, CliqueTree &out, CliqueMap clique_map) {
            typedef typename boost::graph_traits<CliqueTree>::vertex_descriptor CTVertexDescriptor;
            typedef typename boost::property_traits<CliqueMap>::value_type ValueType;
            typedef clique<VertexDescriptor> Clique;

            // Create vertices in the output graph
            std::vector<CTVertexDescriptor> node_vector;
            unsigned int clique_count = cliques.size();

            for (unsigned int i = 0; i < clique_count; i++)
                node_vector.push_back(add_vertex(out));

            // Add edges between vertices
            for (auto clique_it = cliques.rbegin(); clique_it != std::prev(cliques.rend()); clique_it++) {
                CTVertexDescriptor node1 = node_vector[(*clique_it)->list_position_index];
                CTVertexDescriptor node2 = node_vector[(*clique_it)->parent->list_position_index];
                add_edge(node1, node2, out);
            }

            // Add clique property to the vertices
            unsigned int node_vector_index = 0;
            for (Clique *clique : cliques) {
                ValueType new_value = {};

                for (VertexDescriptor vertex : clique->clique_vertices)
                    new_value.insert(new_value.end(), vertex);

                put(clique_map, node_vector[node_vector_index], new_value);

                node_vector_index++;
            }
        }

        // Swaps element/clique to the end of its partition
        template<class ValueType>
        void swap_to_end(ValueType *component) {
            typename std::list<ValueType *>::iterator old_position = component->list_position_it;

            ValueType *tmp = component;
            *old_position = *(component->containing_partition_it->back);
            *(component->containing_partition_it->back) = tmp;

            // Swap position iterators for the two swapped components
            component->list_position_it = component->containing_partition_it->back;
            (*old_position)->list_position_it = old_position;
        }

        // Swaps element/clique to the end of its partition
        template<class ValueType>
        void swap_to_front(ValueType *component) {
            typename std::list<ValueType *>::iterator old_position = component->list_position_it;

            ValueType *tmp = component;
            *old_position = *(component->containing_partition_it->begin);
            *(component->containing_partition_it->begin) = tmp;

            // Swap position iterators for the two swapped components
            component->list_position_it = component->containing_partition_it->begin;
            (*old_position)->list_position_it = old_position;
        }

        // Construct clique_map which will be used for quick lookup of cliques which contain a graph vertex,
        // key is the vertex Lex-BFS ordering value
        template<class VertexDescriptor>
        void construct_clique_map(const std::list<clique<VertexDescriptor> *> &cliques,
                                  std::vector<std::vector<clique<VertexDescriptor> *>> &clique_map, unsigned int N) {
            typedef vertex_element<VertexDescriptor> Element;
            typedef clique<VertexDescriptor> Clique;

            clique_map = std::vector<std::vector<clique<VertexDescriptor> *>>(N);

            for (Clique *clique: cliques) {
                for (Element *element: clique->clique_elements)
                    clique_map[element->ordering_pos].push_back(clique);
            }
        }

        // Add neighbours to unvisited_neighbours used for fast edge removal
        template<class VertexDescriptor>
        void add_unvisited_neighbours(const std::list<clique<VertexDescriptor> *> &cliques) {
            typedef clique<VertexDescriptor> Clique;

            for (Clique *clique:cliques) {
                for (Clique *neighbour :clique->neighbours)
                    clique->unvisited_neighbours.insert(neighbour);
            }
        }

        // Does vertex intersection of two cliques
        template<class VertexDescriptor>
        void cliques_intersection(const clique<VertexDescriptor> *clique1, const clique<VertexDescriptor> *clique2,
                                  std::unordered_set<unsigned int> &new_pivots) {
            auto clique1_it = clique1->clique_elements.begin();
            auto clique2_it = clique2->clique_elements.begin();

            while (clique1_it != clique1->clique_elements.end() && clique2_it != clique2->clique_elements.end()) {
                unsigned int o_pos1 = (*clique1_it)->ordering_pos;
                unsigned int o_pos2 = (*clique2_it)->ordering_pos;

                if (o_pos1 == o_pos2) {
                    new_pivots.insert(o_pos1);
                    clique1_it++;
                    clique2_it++;
                } else if (o_pos1 < o_pos2)
                    clique1_it++;
                else
                    clique2_it++;
            }
        }

        // Finds the first and last partition classes which contain given vertex
        template<class VertexDescriptor>
        void find_fl_occurence(const std::vector<clique<VertexDescriptor> *> &pivot_cliques,
                               const typename std::list<partition_class<clique<VertexDescriptor>>> &partitions,
                               typename std::list<partition_class<clique<VertexDescriptor>>>::iterator &first,
                               typename std::list<partition_class<clique<VertexDescriptor>>>::iterator &last) {
            typedef clique<VertexDescriptor> Clique;
            typedef typename std::list<partition_class<clique<VertexDescriptor>>>::iterator PartitionIt;

            for (Clique *clique : pivot_cliques)
                clique->containing_partition_it->has_pivot = true;

            for (Clique *clique : pivot_cliques) {
                PartitionIt prev = std::prev(clique->containing_partition_it);
                PartitionIt next = std::next(clique->containing_partition_it);

                if (clique->containing_partition_it == partitions.begin() || !prev->has_pivot)
                    first = clique->containing_partition_it;
                else if (next == partitions.end() || !next->has_pivot)
                    last = clique->containing_partition_it;
            }

            for (Clique *clique : pivot_cliques)
                clique->containing_partition_it->has_pivot = false;
        }

        // Finds new pivots and removes edges between cliques from which new pivots were taken
        template<class VertexDescriptor>
        void find_new_pivots(const std::unordered_set<clique<VertexDescriptor> *> &S, std::deque<unsigned int> &q,
                             std::unordered_set<unsigned int> &processed_pivots) {
            typedef clique<VertexDescriptor> Clique;

            std::unordered_set<unsigned int> new_pivots;
            for (Clique *clique : S) {
                std::vector<Clique *> tmp = {};
                for (Clique *tmp_element : clique->unvisited_neighbours)
                    tmp.push_back(tmp_element);

                for (Clique *unvisited_neighbour : tmp) {
                    if (S.find(unvisited_neighbour) != S.end())
                        continue;

                    clique->unvisited_neighbours.erase(unvisited_neighbour);
                    unvisited_neighbour->unvisited_neighbours.erase(clique);

                    cliques_intersection(clique, unvisited_neighbour, new_pivots);
                }
            }

            // Add unprocessed pivots to q
            for (unsigned int x : new_pivots) {
                if (processed_pivots.find(x) == processed_pivots.end()) {
                    q.push_back(x);
                    processed_pivots.insert(x);
                }
            }
        }

        // Final check whether the given clique ordering is a clique chain
        template<class VertexDescriptor>
        bool check_clique_chain(const std::list<clique<VertexDescriptor> *> &cliques,
                                std::map<VertexDescriptor, std::pair<unsigned int, unsigned int>> &intervals,
                                unsigned int N) {
            typedef vertex_element<VertexDescriptor> Element;
            typedef clique<VertexDescriptor> Clique;

            std::vector<unsigned int> occurrence_vector(N, 0);
            unsigned int time_count = 1;

            for (Clique *clique : cliques) {
                for (Element *element: clique->clique_elements) {
                    if (occurrence_vector[element->ordering_pos] != 0 &&
                        (occurrence_vector[element->ordering_pos] + 1) != time_count) {
                        return false;
                    }
                    // Build map of possible intervals for vertices
                    if (occurrence_vector[element->ordering_pos] == 0)
                        intervals.insert(
                                std::make_pair(element->vertex_descriptor,
                                               std::make_pair(time_count * 10, (time_count * 10) + 5)));
                    else
                        intervals[element->vertex_descriptor].second += 10;

                    occurrence_vector[element->ordering_pos] = time_count;
                }
                time_count++;
            }

            return true;
        }

        template<class Container>
        void free_memory(const Container &container) {
            for (auto x : container)
                delete x;
        }

        // Uses bfs to find whether the graph is connected
        template<class Graph>
        bool is_graph_connected(const Graph &g) {
            typedef typename boost::graph_traits<Graph>::vertex_descriptor VertexDescriptor;
            typedef vertex_element<VertexDescriptor> Element;

            std::list<Element *> elements;

            construct_vertex_elements(g, vertices(g).first, vertices(g).second, elements);
            add_neighbours_to_elements(g, elements);

            std::deque<Element *> q;
            q.push_back(*elements.begin());
            unsigned int vertex_count = 0;
            q.front()->visited = true;

            while (!q.empty()) {
                Element *pivot = q.front();
                q.pop_front();
                vertex_count++;

                for (Element *neighbour : pivot->neighbours) {
                    if (!neighbour->visited) {
                        neighbour->visited = true;
                        q.push_back(neighbour);
                    }
                }
            }

            free_memory(elements);

            return (vertex_count == num_vertices(g));
        }

        // Pushes the clique containing first element in Lex-BFS ordering to the back of cliques
        template<class VertexDescriptor>
        void setup_last_clique(std::list<clique<VertexDescriptor> *> &cliques,
                               const std::vector<std::vector<clique<VertexDescriptor> *>> &clique_map) {
            cliques.erase(clique_map[0][0]->list_position_it);
            cliques.push_back(clique_map[0][0]);
            clique_map[0][0]->list_position_it = std::prev(cliques.end());
            clique_map[0][0]->containing_partition_it->back = std::prev(cliques.end());
        }

        template<class Component, class Partition>
        void
        split_partition_to_back(Component *component, std::list<Partition> &partitions, unsigned int current_time) {

            // If the component is not already at the back of it’s partition class, swap it to the end
            if (component->list_position_it != component->containing_partition_it->back)
                swap_to_end(component);

            // If the next partition of the old containing partition of current component
            // is not new (not created for a previous component in current S) or
            // was created by splitting to the front in case of clique partitioning, then create a new partition
            // for current component right after the old one
            typename std::list<Partition>::iterator next_pos_partition_it =
                    std::next(component->containing_partition_it);

            if (next_pos_partition_it == partitions.end() || next_pos_partition_it->time != current_time ||
                next_pos_partition_it->from_right) {
                Partition new_partition = Partition(component->list_position_it,
                                                    component->list_position_it, current_time);
                next_pos_partition_it = partitions.insert(next_pos_partition_it, new_partition);
            }
                // Enlarge the next partition to the front
            else
                (next_pos_partition_it->begin)--;

            // If the partition from which we will take out the current pivot would be empty,
            // erase it from the partitions list
            if (component->containing_partition_it->begin == component->containing_partition_it->back)
                partitions.erase(component->containing_partition_it);
                // Shrink the partition to the left
            else
                component->containing_partition_it->back--;

            // Update current component’s containing partition
            component->containing_partition_it = next_pos_partition_it;
        }

        template<class Component, class Partition>
        void
        split_partition_to_front(Component *component, std::list<Partition> &partitions, unsigned int current_time) {
            // If the component is not already at the front of it’s partition class, swap it to the begin
            if (component->list_position_it != component->containing_partition_it->begin)
                swap_to_front(component);

            // If the previous partition of the old containing partition of current component
            // is not new (not created for a previous component in current S) or
            // was created by splitting to the back, then create a new partition
            // for current component right after the old one
            typename std::list<Partition>::iterator next_pos_partition_it = std::prev(
                    component->containing_partition_it);

            if (next_pos_partition_it->time != current_time || !next_pos_partition_it->from_right) {
                Partition new_partition = Partition(component->list_position_it,
                                                    component->list_position_it, current_time, true);
                next_pos_partition_it = partitions.insert(component->containing_partition_it, new_partition);
            }
                // Enlarge the next partition to the back
            else
                (next_pos_partition_it->back)++;

            // If the partition from which we will take out the current pivot would be empty,
            // erase it from the partitions list
            if (component->containing_partition_it->begin == component->containing_partition_it->back)
                partitions.erase(component->containing_partition_it);
            else
                // Shrink the partition to the right
                component->containing_partition_it->begin++;

            // Update current neighbour’s containing partition
            component->containing_partition_it = next_pos_partition_it;
        }
    }
}

#endif //INTERVAL_GRAPH_RECOGNITION_UTILITYFUNCTIONS_HPP
