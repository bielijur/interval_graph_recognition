#ifndef INTERVAL_GRAPH_RECOGNITION_STRUCTURES_HPP
#define INTERVAL_GRAPH_RECOGNITION_STRUCTURES_HPP

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_iterator.hpp>
#include <list>
#include <vector>
#include <unordered_map>
#include <iostream>
#include <bits/unordered_set.h>

namespace boost {
    namespace internal {
        template<class VertexDescriptor>
        struct partition_class;

        template<class VertexDescriptor>
        struct clique;

        template<class VertexDescriptor>
        struct vertex_element {
            explicit vertex_element(VertexDescriptor vertex_descriptor) : vertex_descriptor(vertex_descriptor) {}

            VertexDescriptor vertex_descriptor;
            typename std::list<vertex_element<VertexDescriptor> *>::iterator list_position_it;
            typename std::list<partition_class<vertex_element<VertexDescriptor>>>::iterator containing_partition_it;
            clique<VertexDescriptor> *containing_clique;
            std::list<vertex_element<VertexDescriptor> *> neighbours;
            std::list<vertex_element<VertexDescriptor> *> RN;
            vertex_element<VertexDescriptor> *parent = nullptr;
            std::list<vertex_element<VertexDescriptor> *> tree_children;

            unsigned int ordering_pos = 0;
            bool visited = false;
        };

        //    template<class Clique>
//    struct clique_partition_class;

        template<class VertexDescriptor>
        struct clique {
            clique(vertex_element<VertexDescriptor> *init_element) {
                clique_elements.push_back(init_element);
                clique_vertices.push_back(init_element->vertex_descriptor);
            }

            typename std::list<clique<VertexDescriptor> *>::iterator list_position_it;
            typename std::list<partition_class<clique<VertexDescriptor>>>::iterator containing_partition_it;
            clique<VertexDescriptor> *parent;
            std::list<clique<VertexDescriptor> *> children;
            std::list<clique<VertexDescriptor> *> neighbours;
            std::unordered_set<clique<VertexDescriptor> *> unvisited_neighbours;
            std::list<vertex_element<VertexDescriptor> *> clique_elements;
            std::list<VertexDescriptor> clique_vertices;
            unsigned int list_position_index = 0;
            bool visited = false;
        };

        template<class Component>
        struct partition_class {
            partition_class(typename std::list<Component *>::iterator begin,
                            typename std::list<Component *>::iterator back, int time, bool from_right = false) :
                    begin(begin), back(back), time(time), from_right(from_right) {};

            partition_class() {}

            typename std::list<Component *>::iterator begin;
            typename std::list<Component *>::iterator back;
            unsigned int time;
            bool from_right;
            bool has_pivot = false;
        };
    }
}
#endif //INTERVAL_GRAPH_RECOGNITION_STRUCTURES_HPP
