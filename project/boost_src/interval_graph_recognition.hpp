#ifndef INTERVAL_GRAPH_RECOGNITION_INTERVALGRAPHRECOGNITION_HPP
#define INTERVAL_GRAPH_RECOGNITION_INTERVALGRAPHRECOGNITION_HPP

///\cond DO_NOT_DOCUMENT
#include <boost/graph/adjacency_list.hpp>
#include <deque>
#include "internal/utility_functions.hpp"
#include "lex_bfs.hpp"
#include "chordality_test.hpp"
#include "clique_t.hpp"
#include "clique_tree.hpp"
#include <boost/concept/assert.hpp>
#include <boost/graph/graph_concepts.hpp>
///\endcond

namespace boost {
    using namespace internal;

    /**
     * @file
     * @brief Determines whether the input graph is an interval graph and if it is, creates suitable intervals for graph vertices
     * @tparam Graph type of input graph structure, must provide Vertex List Graph Concept and Adjacency Graph Concept
     * @tparam IntervalMap type of vertex property map, key type must be Graph vertex descriptor,
     * value type must be assignable with two unsigned ints
     * @param[in] g input graph, must be connected
     * @param[out] interval_map vertex property map of graph <b>g</b>
     * @return <code>true</code> if <b>g<b> is a interval graph, otherwise <code>false</code>
     */
    template<class Graph, typename IntervalMap>
    bool interval_graph_recognition(const Graph &g, IntervalMap interval_map) {
        typedef typename boost::property_traits<IntervalMap>::value_type value_type;
        typedef typename boost::property_traits<IntervalMap>::key_type key_type;
        typedef typename boost::property_traits<IntervalMap>::category category;
        typedef typename boost::graph_traits<Graph>::vertex_descriptor VertexDescriptor;
        typedef vertex_element<VertexDescriptor> Element;
        typedef clique<VertexDescriptor> Clique;
        typedef partition_class<Clique> Partition;

        BOOST_CONCEPT_ASSERT((boost::concepts::VertexListGraphConcept<Graph>));
        BOOST_CONCEPT_ASSERT((boost::concepts::AdjacencyGraphConcept<Graph>));
        BOOST_STATIC_ASSERT((boost::is_property_map<IntervalMap>::value));
        BOOST_STATIC_ASSERT((boost::is_same<key_type, VertexDescriptor>::value));
        BOOST_STATIC_ASSERT((boost::is_convertible<category, boost::writable_property_map_tag>::value));
        BOOST_STATIC_ASSERT((boost::is_constructible<value_type, unsigned int, unsigned int>::value));
        BOOST_STATIC_ASSERT_MSG((boost::is_undirected_graph<Graph>::value), "Graph must be undirected!");

        unsigned int N = num_vertices(g);

        // In case graph has no vertices, return true
        if (N == 0)
            return true;

        // Return false if graph is not connected
        if (!is_graph_connected(g)) {
            std::cerr << "Graph is not connected!" << std::endl;
            return false;
        }

        std::list<VertexDescriptor> o;

        // Get Lex-BFS ordering
        lex_bfs(g, std::inserter(o, o.begin()));

        // Check if input graph is chordal
        if (!chordality_test(g, o.begin(), o.end())) {
            std::cerr << "Graph is not chordal!" << std::endl;
            return false;
        }

        std::list<Element *> elements;
        std::list<Clique *> cliques;

        // Build clique tree
        clique_tree_aux(g, o.begin(), o.end(), cliques, elements);

        // Create an init partition class
        unsigned int current_time = 0;

        std::list<Partition> partitions;
        partitions.push_back(Partition(cliques.begin(), std::prev(cliques.end()), current_time));

        // Set init partition class for all cliques
        for (Clique *clique : cliques)
            clique->containing_partition_it = partitions.begin();

        // Queue for pivots
        std::deque<unsigned int> q;

        // Set for all cliques containing pivot
        std::unordered_set<Clique *> S;

        // Construct clique_map which will be used for quick lookup of cliques which contain pivot
        std::vector<std::vector<Clique *>> clique_map;
        construct_clique_map(cliques, clique_map, N);

        // Push the clique containing first element in Lex-BFS ordering to the back of cliques
        setup_last_clique(cliques, clique_map);

        // Add neighbours to unvisited_neighbours used for fast edge removal
        add_unvisited_neighbours(cliques);

        current_time = 1;

        // Set for all processed pivots
        std::unordered_set<unsigned int> processed_pivots;

        // Flag for ending the algorithm
        bool all_pivots_processed = false;

        while (true) {
            if (q.empty()) {
                // Break when there are no unprocessed pivots
                if (all_pivots_processed)
                    break;

                all_pivots_processed = true;

                // Take the last clique and shrink its old partition class to the left
                Clique *last_clique = *cliques.rbegin();
                last_clique->containing_partition_it->back--;

                // Create a new partition class for the last clique and place it at the end of partitions
                typename std::list<Partition>::iterator next_pos_partition_it = std::next(
                        last_clique->containing_partition_it);

                next_pos_partition_it = partitions.insert(next_pos_partition_it,
                                                          Partition(last_clique->list_position_it,
                                                                    last_clique->list_position_it, current_time, true));

                // Update last clique’s containing partition
                last_clique->containing_partition_it = next_pos_partition_it;

                S = {last_clique};
            } else {
                // Pick an unprocessed pivot
                unsigned pivot = q.front();
                q.pop_front();
                S = {};

                // Insert all cliques containing the pivot to S
                for (Clique *clique: clique_map[pivot])
                    S.insert(clique);

                // Find the first and last partition classes which contain the pivot
                typename std::list<Partition>::iterator first_occurrence;
                typename std::list<Partition>::iterator last_occurrence;
                find_fl_occurence(clique_map[pivot], partitions, first_occurrence, last_occurrence);

                for (Clique *clique : S) {
                    // If clique belongs to the partition class which has first occurrence of pivot
                    if (clique->containing_partition_it == first_occurrence)
                        split_partition_to_back(clique, partitions, current_time);

                    else if (clique->containing_partition_it == last_occurrence)
                        split_partition_to_front(clique, partitions, current_time);
                }
            }

            // Find new pivots and delete edges between cliques from which pivots were taken
            find_new_pivots(S, q, processed_pivots);

            current_time++;
        }

        std::map<VertexDescriptor, std::pair<unsigned int, unsigned int>> intervals;

        // Check if every vertex is in consecutive cliques
        if (!check_clique_chain(cliques, intervals, N)) {
            free_memory(elements);
            free_memory(cliques);
            return false;
        }

        // Put intervals of vertices to the input graph property map
        for (auto it = vertices(g).first; it != vertices(g).second; it++) {
            std::pair<unsigned int, unsigned int> tmp = intervals[*it];
            interval_map[*it] = value_type(tmp.first, tmp.second);
        }

        free_memory(elements);
        free_memory(cliques);

        return true;
    }
}

#endif //INTERVAL_GRAPH_RECOGNITION_INTERVALGRAPHRECOGNITION_H
