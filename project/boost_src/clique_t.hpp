#ifndef INTERVAL_GRAPH_RECOGNITION_CLIQUE_T_HPP
#define INTERVAL_GRAPH_RECOGNITION_CLIQUE_T_HPP

///\cond DO_NOT_DOCUMENT
#include <boost/graph/properties.hpp>
///\endcond

namespace boost {
    /**
     * @file
     * @brief Vertex property type for property maps
     */
    struct clique_t {
        typedef boost::vertex_property_tag kind;
    };
}

#endif //INTERVAL_GRAPH_RECOGNITION_CLIQUE_T_HPP
