#ifndef INTERVAL_GRAPH_RECOGNITION_CHORDALITYTEST_HPP
#define INTERVAL_GRAPH_RECOGNITION_CHORDALITYTEST_HPP

///\cond DO_NOT_DOCUMENT
#include <boost/concept/assert.hpp>
#include <boost/graph/graph_concepts.hpp>
#include "internal/utility_functions.hpp"
///\endcond

namespace boost {
    using namespace internal;

    /**
     * @file
     * @brief Determines whether the input graph is chordal based on the provided Lex-BFS ordering of vertices
     * @tparam Graph type of input graph structure, must provide Vertex List Graph Concept and Adjacency Graph Concept
     * @tparam Iter forward iterator of a container containing Graph vertex descriptors
     * @param[in] g input graph
     * @param[in] o_begin iterator pointing to the beginning of vertex ordering container
     * @param[in] o_end iterator pointing to the end of vertex ordering container
     * @return <code>true</code> if <b>g</b> is a chordal graph, otherwise <code>false</code>
     */

    template<class Graph, class Iter>
    bool chordality_test(const Graph &g, Iter o_begin, Iter o_end) {
        typedef typename boost::graph_traits<Graph>::vertex_descriptor VertexDescriptor;
        typedef vertex_element<VertexDescriptor> Element;

        BOOST_CONCEPT_ASSERT((boost::concepts::VertexListGraphConcept<Graph>));
        BOOST_CONCEPT_ASSERT((boost::concepts::AdjacencyGraphConcept<Graph>));
        BOOST_STATIC_ASSERT_MSG((boost::is_undirected_graph<Graph>::value), "Graph must be undirected!");
        BOOST_STATIC_ASSERT(
                (boost::is_convertible<typename boost::iterator_traversal<Iter>::type, boost::forward_traversal_tag>::value));
        BOOST_STATIC_ASSERT_MSG(
                (boost::is_same<typename std::iterator_traits<Iter>::value_type, VertexDescriptor>::value),
                "Invalid iterator type!");

        // Define list of all vertices wrapped inside vertex_element (element) struct
        std::list<Element *> elements;
        construct_vertex_elements(g, o_begin, o_end, elements);

        // Add neighbours to elements according to graph edges
        add_neighbours_to_elements(g, elements);

        // Add RN to elements according to vertex ordering and neighbours
        add_RN_to_elements(elements);

        // Order RN lists of elements
        order_elements_RN(elements);

        // Check if element’s RN is subset of element’s parent RN
        for (Element *element : elements) {
            if (!is_RN_subset(element, element->parent)) {
                free_memory(elements);
                return false;
            }
        }

        // Delete created elements wrapping the graph vertices
        free_memory(elements);

        return true;
    }
}

#endif //INTERVAL_GRAPH_RECOGNITION_CHORDALITYTEST_HPP
