#ifndef INTERVAL_GRAPH_RECOGNITION_CLIQUETREE_HPP
#define INTERVAL_GRAPH_RECOGNITION_CLIQUETREE_HPP

///\cond DO_NOT_DOCUMENT
#include <boost/concept/assert.hpp>
#include "boost/concept_check.hpp"
#include <boost/graph/graph_concepts.hpp>
#include "internal/utility_functions.hpp"
#include "chordality_test.hpp"
///\endcond

namespace boost {
    using namespace internal;

    /**
     * @file
     * @brief Creates clique tree from graph
     * @tparam Graph type of input graph structure, must provide Vertex List Graph Concept and Adjacency GraphConcept
     * @tparam Iter forward iterator of a container containing Graph vertex descriptors
     * @tparam CliqueTree type of output graph structure, must provide MutableGraphConcept
     * @tparam CliqueMap type of vertex property map with a structure with implemented insert(iterator, value) method,
     * key type must be CliqueTree vertex descriptor
     * @param[in] g input graph, must be connected
     * @param[in] o_begin iterator pointing to the beginning of a container containing Lex-BFS ordering of <b>g</b> vertices
     * @param[in] o_end iterator pointing to the end of a container containing Lex-BFS ordering of g vertices
     * @param[out] out output graph to which vertices and edges will be added in order to create a clique tree representation of graph <b>g</b>
     * @param[out] clique_map vertex property map of graph <b>out<b>
     */

    template<class Graph, class Iter, class CliqueTree, class CliqueMap>
    void clique_tree(const Graph &g, Iter o_begin, Iter o_end, CliqueTree &out, CliqueMap clique_map) {
        typedef typename boost::property_traits<CliqueMap>::key_type key_type;
        typedef typename boost::property_traits<CliqueMap>::category category;
        typedef typename boost::graph_traits<Graph>::vertex_descriptor VertexDescriptor;
        typedef typename boost::graph_traits<CliqueTree>::vertex_descriptor CTVertexDescriptor;
        typedef vertex_element<VertexDescriptor> Element;
        typedef clique<VertexDescriptor> Clique;

        BOOST_CONCEPT_ASSERT((boost::concepts::VertexListGraphConcept<Graph>));
        BOOST_CONCEPT_ASSERT((boost::concepts::AdjacencyGraphConcept<Graph>));
        BOOST_CONCEPT_ASSERT((boost::concepts::MutableGraphConcept<CliqueTree>));
        BOOST_STATIC_ASSERT((boost::is_property_map<CliqueMap>::value));
        BOOST_STATIC_ASSERT((boost::is_same<key_type, CTVertexDescriptor>::value));
        BOOST_STATIC_ASSERT((boost::is_convertible<category, boost::writable_property_map_tag>::value));
        BOOST_STATIC_ASSERT_MSG((boost::is_undirected_graph<Graph>::value), "Graph must be undirected!");
        BOOST_STATIC_ASSERT_MSG((boost::is_undirected_graph<CliqueTree>::value), "Graph must be undirected!");
        BOOST_STATIC_ASSERT_MSG(
                (boost::is_same<typename std::iterator_traits<Iter>::value_type, VertexDescriptor>::value),
                "Invalid iterator type!");
        BOOST_STATIC_ASSERT(
                (boost::is_convertible<typename boost::iterator_traversal<Iter>::type, boost::forward_traversal_tag>::value));

        if (!is_graph_connected(g)) {
            std::cerr << "Graph is not connected!" << std::endl;
            return;
        }

        if (!chordality_test(g, o_begin, o_end)) {
            std::cerr << "Graph is not chordal!" << std::endl;
            return;
        }

        std::list<Element *> elements;
        // Define list of all cliques wrapped inside clique struct
        std::list<Clique *> cliques;

        // Build internal structure for clique tree
        clique_tree_aux(g, o_begin, o_end, cliques, elements);

        // Construct output graph from internal clique tree
        construct_output(cliques, out, clique_map);

        free_memory(elements);
        free_memory(cliques);
    }
}

#endif //BCPROJECT_CLIQUETREE_H
