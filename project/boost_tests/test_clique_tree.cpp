#include <boost/test/unit_test.hpp>

#include "../boost_src/clique_tree.hpp"
#include "../boost_src/clique_t.hpp"
#include "fixtures/fixtures.cpp"

BOOST_AUTO_TEST_SUITE(clique_tree_test_suite)

    BOOST_FIXTURE_TEST_CASE(test_clique_vertices, Graph_fixture) {
        typedef boost::adjacency_list<>::vertex_descriptor VertexDescriptor;
        typedef boost::internal::clique<VertexDescriptor> Clique;
        typedef boost::internal::vertex_element<VertexDescriptor> Element;

        std::list<Element *> elements;
        std::list<Clique *> cliques;
        clique_tree_aux(g, final_ordering_lex.begin(), final_ordering_lex.end(), cliques, elements);

        auto result_cliques_it = final_cliques.begin();
        for (Clique *clique : cliques) {
            BOOST_REQUIRE(result_cliques_it != final_cliques.end());
            BOOST_REQUIRE(*result_cliques_it == clique->clique_vertices);
            result_cliques_it++;
        }

        BOOST_REQUIRE(result_cliques_it == final_cliques.end());

        for (Element *element : elements)
            delete element;

        for (Clique *clique : cliques)
            delete clique;
    }

    BOOST_FIXTURE_TEST_CASE(test_clique_list_vertices, List_graph_fixture) {
        typedef boost::adjacency_list<boost::listS, boost::listS, boost::undirectedS>::vertex_descriptor VertexDescriptor;
        typedef boost::internal::clique<VertexDescriptor> Clique;
        typedef boost::internal::vertex_element<VertexDescriptor> Element;

        std::list<Element *> elements;
        std::list<Clique *> cliques;
        clique_tree_aux(g, final_ordering_lex.begin(), final_ordering_lex.end(), cliques, elements);

        auto result_cliques_it = final_cliques.begin();
        for (Clique *clique : cliques) {
            BOOST_REQUIRE(result_cliques_it != final_cliques.end());
            BOOST_REQUIRE(*result_cliques_it == clique->clique_vertices);
            result_cliques_it++;
        }

        BOOST_REQUIRE(result_cliques_it == final_cliques.end());

        for (Element *element : elements)
            delete element;

        for (Clique *clique : cliques)
            delete clique;
    }

    BOOST_FIXTURE_TEST_CASE(test_clique_tree, Graph_fixture) {
        typedef boost::adjacency_list<>::vertex_descriptor VertexDescriptor;
        typedef boost::internal::clique<VertexDescriptor> Clique;
        typedef boost::internal::vertex_element<VertexDescriptor> Element;

        std::list<Element *> elements;
        std::list<Clique *> cliques;
        clique_tree_aux(g, final_ordering_lex.begin(), final_ordering_lex.end(), cliques, elements);
        typedef boost::adjacency_list<>::vertex_descriptor VertexDescriptor;

        auto cliques_it = cliques.begin();
        BOOST_REQUIRE(cliques_it != cliques.end());

        // check tree for the first clique
        BOOST_REQUIRE((*cliques_it)->clique_vertices == final_cliques[0]);
        auto children_it = (*cliques_it)->children.begin();
        BOOST_REQUIRE(children_it != (*cliques_it)->children.end());
        BOOST_REQUIRE((*children_it)->parent->clique_vertices == final_cliques[0]);
        BOOST_REQUIRE((*children_it)->clique_vertices == final_cliques[1]);
        children_it++;
        BOOST_REQUIRE(children_it == (*cliques_it)->children.end());

        // check tree for second clique
        cliques_it++;
        BOOST_REQUIRE(cliques_it != cliques.end());
        BOOST_REQUIRE((*cliques_it)->clique_vertices == final_cliques[1]);
        children_it = (*cliques_it)->children.begin();
        BOOST_REQUIRE(children_it != (*cliques_it)->children.end());
        BOOST_REQUIRE((*children_it)->parent->clique_vertices == final_cliques[1]);
        BOOST_REQUIRE((*children_it)->clique_vertices == final_cliques[2]);
        children_it++;
        BOOST_REQUIRE(children_it != (*cliques_it)->children.end());
        BOOST_REQUIRE((*children_it)->parent->clique_vertices == final_cliques[1]);
        BOOST_REQUIRE((*children_it)->clique_vertices == final_cliques[3]);
        children_it++;
        BOOST_REQUIRE(children_it == (*cliques_it)->children.end());

        // check tree for third clique
        cliques_it++;
        BOOST_REQUIRE(cliques_it != cliques.end());
        BOOST_REQUIRE((*cliques_it)->clique_vertices == final_cliques[2]);
        children_it = (*cliques_it)->children.begin();
        BOOST_REQUIRE(children_it == (*cliques_it)->children.end());

        // check tree for fourth clique
        cliques_it++;
        BOOST_REQUIRE(cliques_it != cliques.end());
        BOOST_REQUIRE((*cliques_it)->clique_vertices == final_cliques[3]);
        children_it = (*cliques_it)->children.begin();
        BOOST_REQUIRE(children_it != (*cliques_it)->children.end());
        BOOST_REQUIRE((*children_it)->parent->clique_vertices == final_cliques[3]);
        BOOST_REQUIRE((*children_it)->clique_vertices == final_cliques[4]);
        children_it++;
        BOOST_REQUIRE(children_it != (*cliques_it)->children.end());
        BOOST_REQUIRE((*children_it)->parent->clique_vertices == final_cliques[3]);
        BOOST_REQUIRE((*children_it)->clique_vertices == final_cliques[5]);
        children_it++;
        BOOST_REQUIRE(children_it == (*cliques_it)->children.end());

        // check tree for fifth clique
        cliques_it++;
        BOOST_REQUIRE(cliques_it != cliques.end());
        BOOST_REQUIRE((*cliques_it)->clique_vertices == final_cliques[4]);
        children_it = (*cliques_it)->children.begin();
        BOOST_REQUIRE(children_it == (*cliques_it)->children.end());

        // check tree for sixth clique
        cliques_it++;
        BOOST_REQUIRE(cliques_it != cliques.end());
        BOOST_REQUIRE((*cliques_it)->clique_vertices == final_cliques[5]);
        children_it = (*cliques_it)->children.begin();
        BOOST_REQUIRE(children_it == (*cliques_it)->children.end());

        // check that there are no more cliques
        cliques_it++;
        BOOST_REQUIRE(cliques_it == cliques.end());

        for (Element *element : elements)
            delete element;

        for (Clique *clique : cliques)
            delete clique;

    }

    BOOST_FIXTURE_TEST_CASE(test_out, Graph_fixture) {
        typedef boost::property<boost::clique_t, std::list<boost::adjacency_list<>::vertex_descriptor>> CliqueProperty;
        typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, CliqueProperty> OutGraph;
        typedef boost::property_map<OutGraph, boost::clique_t>::type CliqueMap;
        typedef boost::adjacency_list<>::vertex_descriptor VertexDescriptor;
        typedef boost::internal::vertex_element<VertexDescriptor> Element;
        typedef boost::internal::clique<VertexDescriptor> Clique;

        std::list<Element *> elements;
        OutGraph out;
        CliqueMap clique_map = get(boost::clique_t(), out);

        std::list<Clique *> cliques;
        clique_tree_aux(g, final_ordering_lex.begin(), final_ordering_lex.end(), cliques, elements);

        construct_output(cliques, out, clique_map);

        unsigned int index = 0;

        for (auto vertex_it = vertices(out).first; vertex_it != vertices(out).second; vertex_it++) {
            std::list<VertexDescriptor> vertices = boost::get(clique_map, *vertex_it);

            BOOST_REQUIRE(final_cliques[index] == vertices);
            index++;
        }

        for (Element *element : elements)
            delete element;

        for (Clique *clique : cliques)
            delete clique;
    }

    BOOST_FIXTURE_TEST_CASE(test_not_chord_graph, Not_chord_graph_complex) {
        typedef boost::property<boost::clique_t, std::list<boost::adjacency_list<>::vertex_descriptor>> CliqueProperty;
        typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, CliqueProperty> OutGraph;
        typedef boost::property_map<OutGraph, boost::clique_t>::type CliqueMap;

        OutGraph out;
        CliqueMap clique_map = get(boost::clique_t(), out);

        clique_tree(g, final_ordering_lex.begin(), final_ordering_lex.end(), out, clique_map);

        BOOST_REQUIRE(vertices(out).first == vertices(out).second);
    }

    BOOST_FIXTURE_TEST_CASE(test_complete_graph, Complete_graph) {
        typedef boost::property<boost::clique_t, std::list<boost::adjacency_list<>::vertex_descriptor>> CliqueProperty;
        typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, CliqueProperty> OutGraph;
        typedef boost::property_map<OutGraph, boost::clique_t>::type CliqueMap;
        typedef boost::adjacency_list<>::vertex_descriptor VertexDescriptor;

        OutGraph out;
        CliqueMap clique_map = get(boost::clique_t(), out);

        clique_tree(g, final_ordering_lex.begin(), final_ordering_lex.end(), out, clique_map);
        auto vertex_it = vertices(out).first;
        BOOST_REQUIRE(vertex_it != vertices(out).second);

        boost::property_map<OutGraph, boost::clique_t>::type clique_property_type = get(boost::clique_t(), out);
        std::list<VertexDescriptor> graph_vertices = boost::get(clique_property_type, *vertex_it);
        std::unordered_set<VertexDescriptor> vertices_set;
        for (auto vertex : graph_vertices)
            vertices_set.insert(vertex);

        BOOST_REQUIRE(vertices_set == final_cliques[0]);

        vertex_it++;
        BOOST_REQUIRE(vertex_it == vertices(out).second);
    }

    BOOST_FIXTURE_TEST_CASE(test_empty_graph, Empty_graph) {
        typedef boost::property<boost::clique_t, std::list<boost::adjacency_list<>::vertex_descriptor>> CliqueProperty;
        typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, CliqueProperty> OutGraph;
        typedef boost::property_map<OutGraph, boost::clique_t>::type CliqueMap;
        typedef boost::internal::clique<VertexDescriptor> Clique;
        typedef boost::internal::vertex_element<VertexDescriptor> Element;

        std::list<Element *> elements;
        OutGraph out;
        CliqueMap clique_map = get(boost::clique_t(), out);

        std::list<Clique *> cliques;
        clique_tree_aux(g, final_ordering_lex.begin(), final_ordering_lex.end(), cliques, elements);
        BOOST_REQUIRE(cliques.empty());

        construct_output(cliques, out, clique_map);
        BOOST_REQUIRE(vertices(out).first == vertices(out).second);
    }

    BOOST_FIXTURE_TEST_CASE(test_disconnected_graph, Disconnected_graph_fixture) {
        typedef boost::property<boost::clique_t, std::list<boost::adjacency_list<>::vertex_descriptor>> CliqueProperty;
        typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, CliqueProperty> OutGraph;
        typedef boost::property_map<OutGraph, boost::clique_t>::type CliqueMap;
        typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, CliqueProperty> OutGraph;

        OutGraph out;
        CliqueMap clique_map = get(boost::clique_t(), out);

        clique_tree(g, final_ordering_lex.begin(), final_ordering_lex.end(), out, clique_map);

        BOOST_REQUIRE(vertices(out).first == vertices(out).second);
    }
BOOST_AUTO_TEST_SUITE_END()