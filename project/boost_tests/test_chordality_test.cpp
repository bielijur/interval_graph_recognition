#include <boost/test/unit_test.hpp>

#include "../boost_src/chordality_test.hpp"
#include "fixtures/fixtures.cpp"

BOOST_AUTO_TEST_SUITE(chordality_test_test_suite)

    BOOST_FIXTURE_TEST_CASE(test_complex_graph, Graph_fixture) {

        BOOST_REQUIRE(chordality_test(g, final_ordering_lex.begin(), final_ordering_lex.end()));
    }

    BOOST_FIXTURE_TEST_CASE(test_list_complex_graph, List_graph_fixture) {

        BOOST_REQUIRE(chordality_test(g, final_ordering_lex.begin(), final_ordering_lex.end()));
    }

    BOOST_FIXTURE_TEST_CASE(test_not_chordal_graph, Not_chord_graph) {

        BOOST_REQUIRE(!chordality_test(g, final_ordering_lex.begin(), final_ordering_lex.end()));
    }

    BOOST_FIXTURE_TEST_CASE(test_not_chordal_graph_complex, Not_chord_graph_complex) {

        BOOST_REQUIRE(!chordality_test(g, final_ordering_lex.begin(), final_ordering_lex.end()));
    }

    BOOST_FIXTURE_TEST_CASE(test_empty_graph, Empty_graph) {

        BOOST_REQUIRE(chordality_test(g, final_ordering_lex.begin(), final_ordering_lex.end()));
    }

    BOOST_FIXTURE_TEST_CASE(test_complete_graph, Complete_graph) {

        BOOST_REQUIRE(chordality_test(g, final_ordering_lex.begin(), final_ordering_lex.end()));
    }

    BOOST_FIXTURE_TEST_CASE(test_matrix_graph, Matrix_graph_fixture) {

        BOOST_REQUIRE(chordality_test(g, final_ordering_lex.begin(), final_ordering_lex.end()));
    }

    BOOST_FIXTURE_TEST_CASE(test_disconnected_graph, Disconnected_graph_fixture) {

        BOOST_REQUIRE(chordality_test(g, final_ordering_lex.begin(), final_ordering_lex.end()));
    }

    BOOST_FIXTURE_TEST_CASE(test_disconnected_graph_simple, Disconnected_graph_simple) {

        BOOST_REQUIRE(chordality_test(g, final_ordering_lex.begin(), final_ordering_lex.end()));
    }

BOOST_AUTO_TEST_SUITE_END()