#include <boost/test/unit_test.hpp>

#include "../boost_src/interval_graph_recognition.hpp"
#include "../boost_src/construct_interval_graph.hpp"
#include "fixtures/fixtures.cpp"
#include <ctime>

BOOST_AUTO_TEST_SUITE(interval_graph_recognition_test_suite)

    BOOST_FIXTURE_TEST_CASE(test_complex_graph, Graph_fixture) {

        typedef boost::property<boost::vertex_distance_t, std::pair<unsigned int, unsigned int>> IntervalProperty;
        typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, IntervalProperty> Graph;
        typedef boost::property_map<Graph, boost::vertex_distance_t>::type IntervalMap;
        IntervalMap interval_map = get(boost::vertex_distance, g);

        BOOST_REQUIRE(interval_graph_recognition(g, interval_map));

        BOOST_REQUIRE(interval_map[v1] == final_intervals[v1]);
        BOOST_REQUIRE(interval_map[v2] == final_intervals[v2]);
        BOOST_REQUIRE(interval_map[v3] == final_intervals[v3]);
        BOOST_REQUIRE(interval_map[v4] == final_intervals[v4]);
        BOOST_REQUIRE(interval_map[v5] == final_intervals[v5]);
        BOOST_REQUIRE(interval_map[v6] == final_intervals[v6]);
        BOOST_REQUIRE(interval_map[v7] == final_intervals[v7]);
        BOOST_REQUIRE(interval_map[v8] == final_intervals[v8]);
    }

    BOOST_FIXTURE_TEST_CASE(test_list_complex_graph, List_graph_fixture) {

        typedef boost::property<boost::vertex_distance_t, std::pair<unsigned int, unsigned int>> IntervalProperty;
        typedef boost::adjacency_list<boost::listS, boost::listS, boost::undirectedS, IntervalProperty> Graph;
        typedef boost::property_map<Graph, boost::vertex_distance_t>::type IntervalMap;
        IntervalMap interval_map = get(boost::vertex_distance, g);

        BOOST_REQUIRE(interval_graph_recognition(g, interval_map));

        BOOST_REQUIRE(interval_map[v1] == final_intervals[v1]);
        BOOST_REQUIRE(interval_map[v2] == final_intervals[v2]);
        BOOST_REQUIRE(interval_map[v3] == final_intervals[v3]);
        BOOST_REQUIRE(interval_map[v4] == final_intervals[v4]);
        BOOST_REQUIRE(interval_map[v5] == final_intervals[v5]);
        BOOST_REQUIRE(interval_map[v6] == final_intervals[v6]);
        BOOST_REQUIRE(interval_map[v7] == final_intervals[v7]);
        BOOST_REQUIRE(interval_map[v8] == final_intervals[v8]);
    }

    BOOST_FIXTURE_TEST_CASE(test_empty_graph, Empty_graph) {

        typedef boost::property<boost::vertex_distance_t, boost::icl::discrete_interval<unsigned int>> IntervalProperty;
        typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, IntervalProperty> Graph;
        typedef boost::property_map<Graph, boost::vertex_distance_t>::type IntervalMap;
        IntervalMap interval_map = get(boost::vertex_distance, g);

        BOOST_REQUIRE(interval_graph_recognition(g, interval_map));
    }

    BOOST_FIXTURE_TEST_CASE(test_not_chordal_graph, Not_chord_graph) {

        typedef boost::property<boost::vertex_distance_t, boost::icl::discrete_interval<unsigned int>> IntervalProperty;
        typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, IntervalProperty> Graph;
        typedef boost::property_map<Graph, boost::vertex_distance_t>::type IntervalMap;
        IntervalMap interval_map = get(boost::vertex_distance, g);

        BOOST_REQUIRE(!interval_graph_recognition(g, interval_map));
    }

    BOOST_FIXTURE_TEST_CASE(test_complete_graph, Complete_graph) {

        typedef boost::property<boost::vertex_distance_t, boost::icl::discrete_interval<unsigned int>> IntervalProperty;
        typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, IntervalProperty> Graph;
        typedef boost::property_map<Graph, boost::vertex_distance_t>::type IntervalMap;
        IntervalMap interval_map = get(boost::vertex_distance, g);

        BOOST_REQUIRE(interval_graph_recognition(g, interval_map));

        BOOST_REQUIRE(interval_map[v1] == final_intervals[v1]);
        BOOST_REQUIRE(interval_map[v2] == final_intervals[v2]);
        BOOST_REQUIRE(interval_map[v3] == final_intervals[v3]);
        BOOST_REQUIRE(interval_map[v4] == final_intervals[v4]);
        BOOST_REQUIRE(interval_map[v5] == final_intervals[v5]);
        BOOST_REQUIRE(interval_map[v6] == final_intervals[v6]);
    }

    BOOST_FIXTURE_TEST_CASE(test_matrix_graph, Matrix_graph_fixture) {

        typedef boost::property<boost::vertex_distance_t, boost::icl::continuous_interval<double>> IntervalProperty;
        typedef boost::adjacency_matrix<boost::undirectedS, IntervalProperty> Graph;
        typedef boost::property_map<Graph, boost::vertex_distance_t>::type IntervalMap;
        IntervalMap interval_map = get(boost::vertex_distance, g);

        BOOST_REQUIRE(interval_graph_recognition(g, interval_map));

        BOOST_REQUIRE(interval_map[v1] == final_intervals[v1]);
        BOOST_REQUIRE(interval_map[v2] == final_intervals[v2]);
        BOOST_REQUIRE(interval_map[v3] == final_intervals[v3]);
        BOOST_REQUIRE(interval_map[v4] == final_intervals[v4]);
        BOOST_REQUIRE(interval_map[v5] == final_intervals[v5]);
        BOOST_REQUIRE(interval_map[v6] == final_intervals[v6]);
        BOOST_REQUIRE(interval_map[v7] == final_intervals[v7]);
        BOOST_REQUIRE(interval_map[v8] == final_intervals[v8]);
    }

    BOOST_FIXTURE_TEST_CASE(test_not_interval_graph, Not_interval_graph) {

        typedef boost::property<boost::vertex_distance_t, std::pair<unsigned int, unsigned int>> IntervalProperty;
        typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, IntervalProperty> Graph;
        typedef boost::property_map<Graph, boost::vertex_distance_t>::type IntervalMap;
        IntervalMap interval_map = get(boost::vertex_distance, g);

        BOOST_REQUIRE(!interval_graph_recognition(g, interval_map));
    }

    BOOST_FIXTURE_TEST_CASE(test_disconnected_graph, Disconnected_graph_fixture) {

        typedef boost::property<boost::vertex_distance_t, std::pair<unsigned int, unsigned int>> IntervalProperty;
        typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, IntervalProperty> Graph;
        typedef boost::property_map<Graph, boost::vertex_distance_t>::type IntervalMap;
        IntervalMap interval_map = get(boost::vertex_distance, g);

        BOOST_REQUIRE(!interval_graph_recognition(g, interval_map));
    }

    // Generates random graph, change parameter "j" in the first for cycle in order to change the number of tests run
    BOOST_AUTO_TEST_CASE(test1) {
        typedef boost::icl::discrete_interval<unsigned int> Interval;
        typedef boost::property<boost::vertex_distance_t, Interval> IntervalProperty;
        typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, IntervalProperty> Graph;
        typedef boost::property_map<Graph, boost::vertex_distance_t>::type IntervalMap;

        for (unsigned int j = 0; j != 50; j++) {
            Graph g;
            IntervalMap interval_map = get(boost::vertex_distance, g);

            srand((unsigned) time(0));

            std::list<boost::icl::discrete_interval<unsigned int>> interval_list;
            // The upper limit for x value in (x, y)
            unsigned int max_value = rand() % 100 + 1;

            // The maximal number of vertices allowed
            unsigned int vertices_num = rand() % 10 + 1;

            interval_list.emplace_back(
                    boost::icl::discrete_interval<unsigned int>(0, max_value, boost::icl::interval_bounds::closed()));

            // Assign random values to (x, y), where y can is allowed to expand out of the max_value
            for (unsigned int i = 0; i < vertices_num; i++) {
                unsigned int x = rand() % max_value;
                unsigned int y = rand() % (max_value + 100);

                if (x > y)
                    interval_list.emplace_back(
                            boost::icl::discrete_interval<unsigned int>(y, x, boost::icl::interval_bounds::closed()));
                // Swap values if the second value is greater
                else
                    interval_list.emplace_back(
                            boost::icl::discrete_interval<unsigned int>(x, y, boost::icl::interval_bounds::closed()));

                // Update the upper limit for the next values x in (x, y)
                if (y > max_value)
                    max_value = y;
            }

            // Constructs interval graph from list of intervals
            boost::construct_interval_graph(g, interval_list.begin(), interval_list.end(), interval_map);

            BOOST_REQUIRE(interval_graph_recognition(g, interval_map));
        }
    }

BOOST_AUTO_TEST_SUITE_END()