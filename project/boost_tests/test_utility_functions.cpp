#include <boost/test/unit_test.hpp>
#include "../boost_src/internal/utility_functions.hpp"
#include "fixtures/fixtures.cpp"

BOOST_FIXTURE_TEST_SUITE(utility_test_suite, Utility_fixture)

    BOOST_AUTO_TEST_CASE(test_construct_elements) {
        typedef boost::adjacency_list<>::vertex_descriptor VertexDescriptor;
        typedef boost::internal::vertex_element<VertexDescriptor> Element;

        std::list<Element *> elements;
        construct_vertex_elements(graph_fixture.g, graph_fixture.final_ordering_lex.begin(),
                                  graph_fixture.final_ordering_lex.end(), elements);

        unsigned int index = 0;
        for (Element *element: elements) {
            BOOST_REQUIRE(element->vertex_descriptor == graph_fixture.final_ordering_lex[index]);
            index++;
        }

        for (Element *element: elements)
            delete element;
    }

    BOOST_AUTO_TEST_CASE(test_add_neighbours_to_elements) {
        typedef boost::adjacency_list<>::vertex_descriptor VertexDescriptor;
        typedef boost::internal::vertex_element<VertexDescriptor> Element;

        std::list<Element *> elements;
        construct_vertex_elements(graph_fixture.g, graph_fixture.final_ordering_lex.begin(),
                                  graph_fixture.final_ordering_lex.end(), elements);

        add_neighbours_to_elements(graph_fixture.g, elements);

        std::unordered_set<VertexDescriptor> neighbour_vertices;
        unsigned int index = 0;
        for (Element *element: elements) {
            neighbour_vertices = {};
            for (Element *neighbour : element->neighbours) {
                neighbour_vertices.insert(neighbour->vertex_descriptor);
            }
            BOOST_REQUIRE(neighbour_vertices == neighbour_vector[index]);
            index++;
        }

        for (Element *element: elements)
            delete element;
    }

    BOOST_AUTO_TEST_CASE(test_add_RN_to_elements) {
        typedef boost::adjacency_list<>::vertex_descriptor VertexDescriptor;
        typedef boost::internal::vertex_element<VertexDescriptor> Element;

        std::list<Element *> elements;
        construct_vertex_elements(graph_fixture.g, graph_fixture.final_ordering_lex.begin(),
                                  graph_fixture.final_ordering_lex.end(), elements);

        add_neighbours_to_elements(graph_fixture.g, elements);

        add_RN_to_elements(elements);

        std::unordered_set<VertexDescriptor> RN_vertices;
        unsigned int index = 0;
        for (Element *element: elements) {
            RN_vertices = {};
            for (Element *RN_element : element->RN) {
                RN_vertices.insert(RN_element->vertex_descriptor);
            }
            BOOST_REQUIRE(RN_vertices == RN_vector[index]);
            index++;
        }

        for (Element *element: elements)
            delete element;
    }

    BOOST_AUTO_TEST_CASE(test_order_elements_RN) {
        typedef boost::adjacency_list<>::vertex_descriptor VertexDescriptor;
        typedef boost::internal::vertex_element<VertexDescriptor> Element;

        std::list<Element *> elements;
        construct_vertex_elements(graph_fixture.g, graph_fixture.final_ordering_lex.begin(),
                                  graph_fixture.final_ordering_lex.end(), elements);

        add_neighbours_to_elements(graph_fixture.g, elements);

        add_RN_to_elements(elements);
        order_elements_RN(elements);

        std::list<VertexDescriptor> RN_vertices;
        unsigned int index = 0;
        for (Element *element: elements) {
            RN_vertices = {};
            for (Element *RN_element : element->RN) {
                RN_vertices.push_back(RN_element->vertex_descriptor);
            }
            BOOST_REQUIRE(RN_vertices == ordered_RN_vector[index]);
            index++;
        }

        for (Element *element: elements)
            delete element;
    }

    BOOST_AUTO_TEST_CASE(test_is_RN_subset) {
        typedef boost::adjacency_list<>::vertex_descriptor VertexDescriptor;
        typedef boost::internal::vertex_element<VertexDescriptor> Element;

        Element *e0 = new Element(0);
        Element *e1 = new Element(1);
        Element *e2 = new Element(2);
        Element *e3 = new Element(3);
        Element *e4 = new Element(4);
        Element *e5 = new Element(5);
        Element *e6 = new Element(6);

        e0->RN.push_back(e1);
        e0->RN.push_back(e2);
        e0->RN.push_back(e3);
        e0->RN.push_back(e4);

        e1->RN.push_back(e2);
        e1->RN.push_back(e3);
        e1->RN.push_back(e4);
        e1->RN.push_back(e5);

        BOOST_REQUIRE(is_RN_subset(e0, e1));

        e3->RN.push_back(e1);
        e3->RN.push_back(e2);
        e3->RN.push_back(e3);
        e3->RN.push_back(e4);
        e3->RN.push_back(e6);

        BOOST_REQUIRE(!is_RN_subset(e3, e1));

        delete e0;
        delete e1;
        delete e2;
        delete e3;
        delete e4;
        delete e5;
        delete e6;
    }

    BOOST_AUTO_TEST_CASE(test_is_graph_connected_false) {
        Disconnected_graph_fixture f;

        BOOST_REQUIRE(!boost::internal::is_graph_connected(f.g));
    }

    BOOST_AUTO_TEST_CASE(test_is_graph_connected_true) {
        Matrix_graph_fixture f;

        BOOST_REQUIRE(boost::internal::is_graph_connected(f.g));
    }

BOOST_AUTO_TEST_SUITE_END()