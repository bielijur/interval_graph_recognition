#include <boost/test/unit_test.hpp>

#include "../boost_src/lex_bfs.hpp"
#include "fixtures/fixtures.cpp"

BOOST_AUTO_TEST_SUITE(lex_bfs_test_suite)

    BOOST_FIXTURE_TEST_CASE(test_complex_graph, Graph_fixture) {
        typedef boost::adjacency_list<>::vertex_descriptor VertexDescriptor;

        std::vector<VertexDescriptor> o;

        lex_bfs(g, std::inserter(o, o.begin()));

        BOOST_REQUIRE(o == final_ordering_lex);
    }

    BOOST_FIXTURE_TEST_CASE(test_complex_list_graph, List_graph_fixture) {
        typedef boost::adjacency_list<boost::listS, boost::listS, boost::undirectedS>::vertex_descriptor VertexDescriptor;

        std::vector<VertexDescriptor> o;

        lex_bfs(g, std::inserter(o, o.begin()));

        BOOST_REQUIRE(o == final_ordering_lex);
    }

    BOOST_FIXTURE_TEST_CASE(test_not_chordal_graph_complex, Not_chord_graph_complex) {
        typedef boost::adjacency_list<>::vertex_descriptor VertexDescriptor;

        std::list<VertexDescriptor> o;

        lex_bfs(g, std::inserter(o, o.begin()));

        BOOST_REQUIRE(o == final_ordering_lex);
    }

    BOOST_FIXTURE_TEST_CASE(test_not_chordal_graph, Not_chord_graph) {
        typedef boost::adjacency_list<>::vertex_descriptor VertexDescriptor;

        std::list<VertexDescriptor> o;

        lex_bfs(g, std::inserter(o, o.begin()));

        BOOST_REQUIRE(o == final_ordering_lex);
    }

    BOOST_FIXTURE_TEST_CASE(test_empty_graph, Empty_graph) {
        typedef boost::adjacency_list<>::vertex_descriptor VertexDescriptor;

        std::list<VertexDescriptor> o;

        lex_bfs(g, std::inserter(o, o.begin()));

        BOOST_REQUIRE(o == final_ordering_lex);
    }

    BOOST_FIXTURE_TEST_CASE(test_complete_graph, Complete_graph) {
        typedef boost::adjacency_list<>::vertex_descriptor VertexDescriptor;

        std::list<VertexDescriptor> o;

        lex_bfs(g, std::inserter(o, o.begin()));

        BOOST_REQUIRE(o == final_ordering_lex);
    }

    BOOST_FIXTURE_TEST_CASE(test_matrix_graph, Matrix_graph_fixture) {
        typedef boost::adjacency_matrix<boost::undirectedS>::vertex_descriptor VertexDescriptor;

        std::vector<VertexDescriptor> o;

        lex_bfs(g, std::inserter(o, o.begin()));

        BOOST_REQUIRE(o == final_ordering_lex);
    }

    BOOST_FIXTURE_TEST_CASE(test_disconnected_graph, Disconnected_graph_fixture) {
        typedef boost::adjacency_list<>::vertex_descriptor VertexDescriptor;

        std::vector<VertexDescriptor> o;

        lex_bfs(g, std::inserter(o, o.begin()));

        BOOST_REQUIRE(o == final_ordering_lex);
    }

    BOOST_FIXTURE_TEST_CASE(test_disconnected_graph_simple, Disconnected_graph_simple) {
        typedef boost::adjacency_list<>::vertex_descriptor VertexDescriptor;

        std::vector<VertexDescriptor> o;

        lex_bfs(g, std::inserter(o, o.begin()));

        BOOST_REQUIRE(o == final_ordering_lex);
    }

BOOST_AUTO_TEST_SUITE_END()