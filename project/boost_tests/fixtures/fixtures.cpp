#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/adjacency_matrix.hpp>
#include <boost/icl/discrete_interval.hpp>
#include <boost/icl/continuous_interval.hpp>

// Complex graph
struct Graph_fixture {
    typedef boost::adjacency_list<>::vertex_descriptor VertexDescriptor;
    VertexDescriptor v1, v2, v3, v4, v5, v6, v7, v8;

    typedef boost::property<boost::vertex_distance_t, std::pair<unsigned int, unsigned int>> IntervalProperty;
    boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, IntervalProperty> g;

    std::vector<VertexDescriptor> final_ordering_lex;
    std::vector<std::list<VertexDescriptor>> final_cliques;
    std::map<VertexDescriptor, std::pair<unsigned int, unsigned int>> final_intervals;

    Graph_fixture() {
        v1 = boost::add_vertex(g);
        v2 = boost::add_vertex(g);
        v3 = boost::add_vertex(g);
        v4 = boost::add_vertex(g);
        v5 = boost::add_vertex(g);
        v6 = boost::add_vertex(g);
        v7 = boost::add_vertex(g);
        v8 = boost::add_vertex(g);

        boost::add_edge(v4, v8, g);
        boost::add_edge(v7, v8, g);
        boost::add_edge(v3, v8, g);
        boost::add_edge(v4, v7, g);
        boost::add_edge(v5, v8, g);
        boost::add_edge(v1, v6, g);
        boost::add_edge(v2, v8, g);
        boost::add_edge(v6, v7, g);
        boost::add_edge(v3, v6, g);
        boost::add_edge(v5, v7, g);
        boost::add_edge(v6, v8, g);

        final_ordering_lex = {v1, v2, v3, v5, v6, v7, v4, v8};
        final_cliques = {{v2, v8},
                         {v7, v4, v8},
                         {v5, v7, v8},
                         {v6, v7, v8},
                         {v1, v6},
                         {v3, v6, v8}};

        final_intervals[v1] = std::make_pair(60, 65);
        final_intervals[v2] = std::make_pair(10, 15);
        final_intervals[v3] = std::make_pair(50, 55);
        final_intervals[v4] = std::make_pair(20, 25);
        final_intervals[v5] = std::make_pair(30, 35);
        final_intervals[v6] = std::make_pair(40, 65);
        final_intervals[v7] = std::make_pair(20, 45);
        final_intervals[v8] = std::make_pair(10, 55);
    };

    ~Graph_fixture() = default;
};

struct List_graph_fixture {
    typedef boost::adjacency_list<boost::listS, boost::listS, boost::undirectedS>::vertex_descriptor VertexDescriptor;
    VertexDescriptor v1, v2, v3, v4, v5, v6, v7, v8;

    typedef boost::property<boost::vertex_distance_t, std::pair<unsigned int, unsigned int>> IntervalProperty;
    boost::adjacency_list<boost::listS, boost::listS, boost::undirectedS, IntervalProperty> g;

    std::vector<VertexDescriptor> final_ordering_lex;
    std::vector<std::list<VertexDescriptor>> final_cliques;
    std::map<VertexDescriptor, std::pair<unsigned int, unsigned int>> final_intervals;


    List_graph_fixture() {
        v1 = boost::add_vertex(g);
        v2 = boost::add_vertex(g);
        v3 = boost::add_vertex(g);
        v4 = boost::add_vertex(g);
        v5 = boost::add_vertex(g);
        v6 = boost::add_vertex(g);
        v7 = boost::add_vertex(g);
        v8 = boost::add_vertex(g);

        boost::add_edge(v4, v8, g);
        boost::add_edge(v7, v8, g);
        boost::add_edge(v3, v8, g);
        boost::add_edge(v4, v7, g);
        boost::add_edge(v5, v8, g);
        boost::add_edge(v1, v6, g);
        boost::add_edge(v2, v8, g);
        boost::add_edge(v6, v7, g);
        boost::add_edge(v3, v6, g);
        boost::add_edge(v5, v7, g);
        boost::add_edge(v6, v8, g);

        final_ordering_lex = {v1, v2, v3, v5, v6, v7, v4, v8};
        final_cliques = {{v2, v8},
                         {v7, v4, v8},
                         {v5, v7, v8},
                         {v6, v7, v8},
                         {v1, v6},
                         {v3, v6, v8}};

        final_intervals[v1] = std::make_pair(60, 65);
        final_intervals[v2] = std::make_pair(10, 15);
        final_intervals[v3] = std::make_pair(50, 55);
        final_intervals[v4] = std::make_pair(20, 25);
        final_intervals[v5] = std::make_pair(30, 35);
        final_intervals[v6] = std::make_pair(40, 65);
        final_intervals[v7] = std::make_pair(20, 45);
        final_intervals[v8] = std::make_pair(10, 55);
    };

    ~List_graph_fixture() = default;
};

// Not a chordal graph
struct Not_chord_graph {
    typedef boost::adjacency_list<>::vertex_descriptor VertexDescriptor;
    VertexDescriptor v1, v2, v3, v4;

    typedef boost::property<boost::vertex_distance_t, boost::icl::discrete_interval<unsigned int>> IntervalProperty;
    boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, IntervalProperty> g;

    std::list<VertexDescriptor> final_ordering_lex;

    Not_chord_graph() {
        v1 = boost::add_vertex(g);
        v2 = boost::add_vertex(g);
        v3 = boost::add_vertex(g);
        v4 = boost::add_vertex(g);

        boost::add_edge(v1, v2, g);
        boost::add_edge(v1, v4, g);
        boost::add_edge(v3, v2, g);
        boost::add_edge(v3, v4, g);

        final_ordering_lex = {v2, v3, v1, v4};
    }
};

struct Not_chord_graph_complex {
    typedef boost::adjacency_list<>::vertex_descriptor VertexDescriptor;
    VertexDescriptor v1, v2, v3, v4, v5, v6;

    typedef boost::property<boost::vertex_distance_t, boost::icl::discrete_interval<unsigned int>> IntervalProperty;
    boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, IntervalProperty> g;

    std::list<VertexDescriptor> final_ordering_lex;

    Not_chord_graph_complex() {
        v1 = boost::add_vertex(g);
        v2 = boost::add_vertex(g);
        v3 = boost::add_vertex(g);
        v4 = boost::add_vertex(g);
        v5 = boost::add_vertex(g);
        v6 = boost::add_vertex(g);

        boost::add_edge(v1, v2, g);
        boost::add_edge(v2, v3, g);
        boost::add_edge(v3, v4, g);
        boost::add_edge(v4, v5, g);
        boost::add_edge(v5, v6, g);
        boost::add_edge(v6, v1, g);
        boost::add_edge(v6, v3, g);

        final_ordering_lex = {v2, v4, v3, v1, v5, v6};
    }
};

// Empty graph without vertices
struct Empty_graph {
    typedef boost::adjacency_list<>::vertex_descriptor VertexDescriptor;

    typedef boost::property<boost::vertex_distance_t, boost::icl::discrete_interval<unsigned int>> IntervalProperty;
    boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, IntervalProperty> g;

    std::list<VertexDescriptor> final_ordering_lex;
    std::vector<std::unordered_set<VertexDescriptor>> final_cliques;

    Empty_graph() {
        final_ordering_lex = {};
        final_cliques = {};
    }
};

// Complete graph
struct Complete_graph {
    typedef boost::adjacency_list<>::vertex_descriptor VertexDescriptor;
    VertexDescriptor v1, v2, v3, v4, v5, v6;

    typedef boost::property<boost::vertex_distance_t, boost::icl::discrete_interval<unsigned int>> IntervalProperty;
    boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, IntervalProperty> g;

    std::list<VertexDescriptor> final_ordering_lex;
    std::vector<std::unordered_set<VertexDescriptor>> final_cliques;
    std::map<VertexDescriptor, boost::icl::discrete_interval<unsigned int>> final_intervals;

    Complete_graph() {
        v1 = boost::add_vertex(g);
        v2 = boost::add_vertex(g);
        v3 = boost::add_vertex(g);
        v4 = boost::add_vertex(g);
        v5 = boost::add_vertex(g);
        v6 = boost::add_vertex(g);


        boost::add_edge(v1, v5, g);
        boost::add_edge(v3, v4, g);
        boost::add_edge(v2, v4, g);
        boost::add_edge(v5, v6, g);
        boost::add_edge(v1, v2, g);
        boost::add_edge(v1, v6, g);
        boost::add_edge(v4, v5, g);
        boost::add_edge(v1, v4, g);
        boost::add_edge(v4, v6, g);
        boost::add_edge(v1, v3, g);
        boost::add_edge(v2, v3, g);
        boost::add_edge(v2, v6, g);
        boost::add_edge(v3, v5, g);
        boost::add_edge(v3, v6, g);
        boost::add_edge(v2, v5, g);

        final_ordering_lex = {v3, v4, v2, v1, v5, v6};
        final_cliques = {{v1, v2, v3, v4, v5, v6}};
        final_intervals[v1] = boost::icl::discrete_interval<unsigned int>(10, 15);
        final_intervals[v2] = boost::icl::discrete_interval<unsigned int>(10, 15);
        final_intervals[v3] = boost::icl::discrete_interval<unsigned int>(10, 15);
        final_intervals[v4] = boost::icl::discrete_interval<unsigned int>(10, 15);
        final_intervals[v5] = boost::icl::discrete_interval<unsigned int>(10, 15);
        final_intervals[v6] = boost::icl::discrete_interval<unsigned int>(10, 15);
    }
};

// Same graph as Graph_fixture, represented as adjacency_matrix
struct Matrix_graph_fixture {
    enum {
        v1, v2, v3, v4, v5, v6, v7, v8, N
    };

    typedef boost::property<boost::vertex_distance_t, boost::icl::continuous_interval<double>> IntervalProperty;
    boost::adjacency_matrix<boost::undirectedS, IntervalProperty> g =
            boost::adjacency_matrix<boost::undirectedS, IntervalProperty>(N);
    std::vector<boost::adjacency_matrix<>::vertex_descriptor> final_ordering_lex;
    std::map<unsigned int, boost::icl::continuous_interval<double>> final_intervals;

    Matrix_graph_fixture() {
        boost::add_edge(v4, v8, g);
        boost::add_edge(v7, v8, g);
        boost::add_edge(v5, v7, g);
        boost::add_edge(v4, v7, g);
        boost::add_edge(v3, v6, g);
        boost::add_edge(v3, v8, g);
        boost::add_edge(v6, v8, g);
        boost::add_edge(v1, v6, g);
        boost::add_edge(v5, v8, g);
        boost::add_edge(v2, v8, g);
        boost::add_edge(v6, v7, g);

        final_ordering_lex = {v1, v5, v4, v7, v6, v3, v2, v8};

        final_intervals[v1] = boost::icl::continuous_interval<double>(60, 65);
        final_intervals[v2] = boost::icl::continuous_interval<double>(10, 15);
        final_intervals[v3] = boost::icl::continuous_interval<double>(50, 55);
        final_intervals[v4] = boost::icl::continuous_interval<double>(30, 35);
        final_intervals[v5] = boost::icl::continuous_interval<double>(20, 25);
        final_intervals[v6] = boost::icl::continuous_interval<double>(40, 65);
        final_intervals[v7] = boost::icl::continuous_interval<double>(20, 45);
        final_intervals[v8] = boost::icl::continuous_interval<double>(10, 55);
    };

    ~Matrix_graph_fixture() = default;
};

struct Not_interval_graph {
    typedef boost::adjacency_list<>::vertex_descriptor VertexDescriptor;
    VertexDescriptor v1, v2, v3, v4, v5, v6, v7;

    typedef boost::property<boost::vertex_distance_t, std::pair<unsigned int, unsigned int>> IntervalProperty;
    boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, IntervalProperty> g;

    Not_interval_graph() {
        v1 = boost::add_vertex(g);
        v2 = boost::add_vertex(g);
        v3 = boost::add_vertex(g);
        v4 = boost::add_vertex(g);
        v5 = boost::add_vertex(g);
        v6 = boost::add_vertex(g);
        v7 = boost::add_vertex(g);

        boost::add_edge(v1, v2, g);
        boost::add_edge(v2, v3, g);
        boost::add_edge(v3, v4, g);
        boost::add_edge(v4, v5, g);
        boost::add_edge(v3, v6, g);
        boost::add_edge(v6, v7, g);

    };

    ~Not_interval_graph() = default;
};

struct Disconnected_graph_fixture {
    typedef boost::adjacency_list<>::vertex_descriptor VertexDescriptor;
    VertexDescriptor v1, v2, v3, v4, v5, v6, v7, v8;

    typedef boost::property<boost::vertex_distance_t, std::pair<unsigned int, unsigned int>> IntervalProperty;
    boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, IntervalProperty> g;

    std::vector<VertexDescriptor> final_ordering_lex;
    std::vector<std::list<VertexDescriptor>> final_cliques;

    Disconnected_graph_fixture() {
        v1 = boost::add_vertex(g);
        v2 = boost::add_vertex(g);
        v3 = boost::add_vertex(g);
        v4 = boost::add_vertex(g);
        v5 = boost::add_vertex(g);
        v6 = boost::add_vertex(g);
        v7 = boost::add_vertex(g);
        v8 = boost::add_vertex(g);

        boost::add_edge(v7, v8, g);
        boost::add_edge(v3, v8, g);
        boost::add_edge(v5, v8, g);
        boost::add_edge(v6, v7, g);
        boost::add_edge(v3, v6, g);
        boost::add_edge(v5, v7, g);
        boost::add_edge(v6, v8, g);

        final_ordering_lex = {v1, v2, v4, v3, v5, v6, v7, v8};
    };

    ~Disconnected_graph_fixture() = default;
};

struct Disconnected_graph_simple {
    typedef boost::adjacency_list<>::vertex_descriptor VertexDescriptor;
    VertexDescriptor v1, v2, v3, v4, v5, v6;

    boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS> g;

    std::vector<VertexDescriptor> final_ordering_lex;

    Disconnected_graph_simple() {
        v1 = boost::add_vertex(g);
        v2 = boost::add_vertex(g);
        v3 = boost::add_vertex(g);
        v4 = boost::add_vertex(g);
        v5 = boost::add_vertex(g);
        v6 = boost::add_vertex(g);

        boost::add_edge(v1, v3, g);
        boost::add_edge(v1, v5, g);
        boost::add_edge(v3, v5, g);
        boost::add_edge(v2, v4, g);
        boost::add_edge(v2, v6, g);
        boost::add_edge(v4, v6, g);


        final_ordering_lex = {v5, v1, v3, v4, v2, v6};

    };

    ~Disconnected_graph_simple() = default;
};

// Fixture for testing utility functions, according to Graph_fixture
struct Utility_fixture {
    typedef boost::adjacency_list<>::vertex_descriptor VertexDescriptor;

    Graph_fixture graph_fixture;

    std::vector<std::unordered_set<VertexDescriptor>> neighbour_vector = {{graph_fixture.v6},
                                                                          {graph_fixture.v8},
                                                                          {graph_fixture.v6, graph_fixture.v8},
                                                                          {graph_fixture.v7, graph_fixture.v8},
                                                                          {graph_fixture.v1, graph_fixture.v3, graph_fixture.v7,
                                                                                  graph_fixture.v8},
                                                                          {graph_fixture.v4, graph_fixture.v5, graph_fixture.v6,
                                                                                  graph_fixture.v8},
                                                                          {graph_fixture.v7, graph_fixture.v8},
                                                                          {graph_fixture.v2, graph_fixture.v3, graph_fixture.v4,
                                                                                  graph_fixture.v5, graph_fixture.v6,
                                                                                  graph_fixture.v7}};

    std::vector<std::unordered_set<VertexDescriptor>> RN_vector = {{graph_fixture.v6},
                                                                   {graph_fixture.v8},
                                                                   {graph_fixture.v6, graph_fixture.v8},
                                                                   {graph_fixture.v7, graph_fixture.v8},
                                                                   {graph_fixture.v7, graph_fixture.v8},
                                                                   {graph_fixture.v4, graph_fixture.v8},
                                                                   {graph_fixture.v8},
                                                                   {}};

    std::vector<std::list<VertexDescriptor>> ordered_RN_vector = {{graph_fixture.v6},
                                                                  {graph_fixture.v8},
                                                                  {graph_fixture.v6, graph_fixture.v8},
                                                                  {graph_fixture.v7, graph_fixture.v8},
                                                                  {graph_fixture.v7, graph_fixture.v8},
                                                                  {graph_fixture.v4, graph_fixture.v8},
                                                                  {graph_fixture.v8},
                                                                  {}};
};

struct Interval_graph_fixture {
    typedef boost::icl::discrete_interval<unsigned int> Interval;
    typedef boost::property<boost::vertex_distance_t, Interval> IntervalProperty;

    boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, IntervalProperty> g;
    std::vector<Interval> interval_list;

    Interval_graph_fixture() {
        interval_list.emplace_back(60, 65, boost::icl::interval_bounds::closed());
        interval_list.emplace_back(10, 15, boost::icl::interval_bounds::closed());
        interval_list.emplace_back(50, 55, boost::icl::interval_bounds::closed());
        interval_list.emplace_back(20, 25, boost::icl::interval_bounds::closed());
        interval_list.emplace_back(30, 35, boost::icl::interval_bounds::closed());
        interval_list.emplace_back(40, 65, boost::icl::interval_bounds::closed());
        interval_list.emplace_back(20, 45, boost::icl::interval_bounds::closed());
        interval_list.emplace_back(10, 55, boost::icl::interval_bounds::closed());
    }
};

struct Empty_interval_graph {
    typedef boost::icl::discrete_interval<unsigned int> Interval;
    typedef boost::property<boost::vertex_distance_t, Interval> IntervalProperty;

    boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, IntervalProperty> g;
    std::vector<Interval> interval_list = {};

    Empty_interval_graph() {}
};

struct Complete_interval_graph {
    typedef boost::icl::continuous_interval<double> Interval;
    typedef boost::property<boost::vertex_distance_t, Interval> IntervalProperty;

    boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, IntervalProperty> g;
    std::vector<Interval> interval_list;

    Complete_interval_graph() {
        interval_list.emplace_back(0, 5, boost::icl::interval_bounds::closed());
        interval_list.emplace_back(0, 4, boost::icl::interval_bounds::closed());
        interval_list.emplace_back(0, 3, boost::icl::interval_bounds::closed());
        interval_list.emplace_back(0, 2, boost::icl::interval_bounds::closed());
        interval_list.emplace_back(0, 1, boost::icl::interval_bounds::closed());
        interval_list.emplace_back(0, 0, boost::icl::interval_bounds::closed());
    }
};

struct Complete_interval_graph2 {
    typedef boost::icl::continuous_interval<double> Interval;
    typedef boost::property<boost::vertex_distance_t, Interval> IntervalProperty;

    boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, IntervalProperty> g;
    std::list<Interval> interval_list;

    Complete_interval_graph2() {
        interval_list.emplace_back(0, 0, boost::icl::interval_bounds::closed());
        interval_list.emplace_back(0, 0, boost::icl::interval_bounds::closed());
        interval_list.emplace_back(0, 0, boost::icl::interval_bounds::closed());
        interval_list.emplace_back(0, 0, boost::icl::interval_bounds::closed());
        interval_list.emplace_back(0, 0, boost::icl::interval_bounds::closed());
        interval_list.emplace_back(0, 0, boost::icl::interval_bounds::closed());
    }
};

struct Disconnected_interval_graph {
    typedef boost::icl::discrete_interval<unsigned int> Interval;
    typedef boost::property<boost::vertex_distance_t, Interval> IntervalProperty;

    boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, IntervalProperty> g;
    std::vector<Interval> interval_list;

    Disconnected_interval_graph() {
        interval_list.emplace_back(7, 7, boost::icl::interval_bounds::closed());
        interval_list.emplace_back(8, 8, boost::icl::interval_bounds::closed());
        interval_list.emplace_back(5, 5, boost::icl::interval_bounds::closed());
        interval_list.emplace_back(2, 2, boost::icl::interval_bounds::closed());
        interval_list.emplace_back(9, 9, boost::icl::interval_bounds::closed());
        interval_list.emplace_back(4, 6, boost::icl::interval_bounds::closed());
        interval_list.emplace_back(2, 4, boost::icl::interval_bounds::closed());
        interval_list.emplace_back(1, 5, boost::icl::interval_bounds::closed());
    }
};