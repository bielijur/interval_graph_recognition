#include <boost/test/unit_test.hpp>

#include "../boost_src/construct_interval_graph.hpp"
#include "fixtures/fixtures.cpp"
#include <boost/graph/isomorphism.hpp>

using namespace boost;
BOOST_AUTO_TEST_SUITE(interval_graph_test_suite)
    BOOST_FIXTURE_TEST_CASE(test_complex_graph, Interval_graph_fixture) {
        typedef boost::icl::discrete_interval<unsigned int> Interval;
        typedef boost::property<boost::vertex_distance_t, Interval> IntervalProperty;
        typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, IntervalProperty> Graph;
        typedef boost::property_map<Graph, boost::vertex_distance_t>::type IntervalMap;
        IntervalMap interval_map = get(boost::vertex_distance, g);

        construct_interval_graph(g, interval_list.begin(), interval_list.end(), interval_map);
        auto orig_graph = Graph_fixture().g;

        BOOST_REQUIRE(num_vertices(g) == 8);
        BOOST_REQUIRE(num_edges(g) == 11);
        BOOST_REQUIRE(boost::isomorphism(orig_graph, g));

        BOOST_REQUIRE(interval_map[0] == interval_list[0]);
        BOOST_REQUIRE(interval_map[1] == interval_list[1]);
        BOOST_REQUIRE(interval_map[2] == interval_list[2]);
        BOOST_REQUIRE(interval_map[3] == interval_list[3]);
        BOOST_REQUIRE(interval_map[4] == interval_list[4]);
        BOOST_REQUIRE(interval_map[5] == interval_list[5]);
        BOOST_REQUIRE(interval_map[6] == interval_list[6]);
        BOOST_REQUIRE(interval_map[7] == interval_list[7]);
    }

    BOOST_FIXTURE_TEST_CASE(test_empty_graph, Empty_interval_graph) {
        typedef boost::icl::discrete_interval<unsigned int> Interval;
        typedef boost::property<boost::vertex_distance_t, Interval> IntervalProperty;
        typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, IntervalProperty> Graph;
        typedef boost::property_map<Graph, boost::vertex_distance_t>::type IntervalMap;
        IntervalMap interval_map = get(boost::vertex_distance, g);

        construct_interval_graph(g, interval_list.begin(), interval_list.end(), interval_map);
        auto orig_graph = Empty_graph().g;

        BOOST_REQUIRE(num_vertices(g) == 0);
        BOOST_REQUIRE(num_edges(g) == 0);
        BOOST_REQUIRE(boost::isomorphism(orig_graph, g));
    }

    BOOST_FIXTURE_TEST_CASE(test_complete_graph, Complete_interval_graph) {
        typedef boost::icl::continuous_interval<double> Interval;
        typedef boost::property<boost::vertex_distance_t, Interval> IntervalProperty;
        typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, IntervalProperty> Graph;
        typedef boost::property_map<Graph, boost::vertex_distance_t>::type IntervalMap;
        IntervalMap interval_map = get(boost::vertex_distance, g);

        construct_interval_graph(g, interval_list.begin(), interval_list.end(), interval_map);
        auto orig_graph = Complete_graph().g;

        BOOST_REQUIRE(num_vertices(g) == 6);
        BOOST_REQUIRE(num_edges(g) == 15);
        BOOST_REQUIRE(boost::isomorphism(orig_graph, g));

        BOOST_REQUIRE(interval_map[0] == interval_list[0]);
        BOOST_REQUIRE(interval_map[1] == interval_list[1]);
        BOOST_REQUIRE(interval_map[2] == interval_list[2]);
        BOOST_REQUIRE(interval_map[3] == interval_list[3]);
        BOOST_REQUIRE(interval_map[4] == interval_list[4]);
        BOOST_REQUIRE(interval_map[5] == interval_list[5]);
    }

    BOOST_FIXTURE_TEST_CASE(test_complete_graph2, Complete_interval_graph2) {
        typedef boost::icl::continuous_interval<double> Interval;
        typedef boost::property<boost::vertex_distance_t, Interval> IntervalProperty;
        typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, IntervalProperty> Graph;
        typedef boost::property_map<Graph, boost::vertex_distance_t>::type IntervalMap;
        IntervalMap interval_map = get(boost::vertex_distance, g);

        construct_interval_graph(g, interval_list.begin(), interval_list.end(), interval_map);
        auto orig_graph = Complete_graph().g;

        BOOST_REQUIRE(num_vertices(g) == 6);
        BOOST_REQUIRE(num_edges(g) == 15);
        BOOST_REQUIRE(boost::isomorphism(orig_graph, g));
    }

    BOOST_FIXTURE_TEST_CASE(test_disconnected_graph, Disconnected_interval_graph) {
        typedef boost::icl::discrete_interval<unsigned int> Interval;
        typedef boost::property<boost::vertex_distance_t, Interval> IntervalProperty;
        typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, IntervalProperty> Graph;
        typedef boost::property_map<Graph, boost::vertex_distance_t>::type IntervalMap;
        IntervalMap interval_map = get(boost::vertex_distance, g);

        construct_interval_graph(g, interval_list.begin(), interval_list.end(), interval_map);
        auto orig_graph = Disconnected_graph_fixture().g;

        BOOST_REQUIRE(num_vertices(g) == 8);
        BOOST_REQUIRE(num_edges(g) == 7);
        BOOST_REQUIRE(boost::isomorphism(orig_graph, g));
    }


BOOST_AUTO_TEST_SUITE_END()