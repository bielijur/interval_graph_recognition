# Interval Graph Recognition support for Boost Graph Library

./project contains whole implementation

./project./boost_src contains hpp files which serve as extension of Boost Graph Library

./project./boost_test contains tests for functions in boost_src, can be run with included makefile using command: make
	
./project./makefile

./doc/html generated documentation using Doxygen

