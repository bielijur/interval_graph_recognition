CXXFLAGS = -std=c++14 -Wall -pedantic
LDFLAGS = -Wl,--no-as-needed -L. -lboost_unit_test_framework

all: run
	
run: compile
	./program

clean:
	rm *.o

compile: test_main.o test_lex_bfs.o test_chordality_test.o test_clique_tree.o test_interval_graph_recognition.o test_construct_interval_graph.o test_utility_functions.o fixtures.o
	g++ test_main.o test_lex_bfs.o test_chordality_test.o test_clique_tree.o test_interval_graph_recognition.o test_construct_interval_graph.o test_utility_functions.o fixtures.o $(LDFLAGS) -o program	

fixtures.o: ./boost_tests/fixtures/fixtures.cpp 
	g++ $(CXXFLAGS) -c ./boost_tests/fixtures/fixtures.cpp -o fixtures.o

test_main.o: ./boost_tests/main_test.cpp
	g++ $(CXXFLAGS) -c ./boost_tests/main_test.cpp -o test_main.o

test_lex_bfs.o: ./boost_tests/test_lex_bfs.cpp ./boost_tests/fixtures/fixtures.cpp 
	g++ $(CXXFLAGS) -c ./boost_tests/test_lex_bfs.cpp -o test_lex_bfs.o

test_chordality_test.o: ./boost_tests/test_chordality_test.cpp ./boost_tests/fixtures/fixtures.cpp 
	g++ $(CXXFLAGS) -c ./boost_tests/test_chordality_test.cpp -o test_chordality_test.o

test_clique_tree.o: ./boost_tests/test_clique_tree.cpp ./boost_tests/fixtures/fixtures.cpp 
	g++ $(CXXFLAGS) -c ./boost_tests/test_clique_tree.cpp -o test_clique_tree.o

test_interval_graph_recognition.o: ./boost_tests/test_interval_graph_recognition.cpp ./boost_tests/fixtures/fixtures.cpp 
	g++ $(CXXFLAGS) -c ./boost_tests/test_interval_graph_recognition.cpp -o test_interval_graph_recognition.o

test_construct_interval_graph.o: ./boost_tests/test_construct_interval_graph.cpp ./boost_tests/fixtures/fixtures.cpp 
	g++ $(CXXFLAGS) -c ./boost_tests/test_construct_interval_graph.cpp -o test_construct_interval_graph.o

test_utility_functions.o: ./boost_tests/test_utility_functions.cpp ./boost_tests/fixtures/fixtures.cpp
	g++ $(CXXFLAGS) -c ./boost_tests/test_utility_functions.cpp -o test_utility_functions.o